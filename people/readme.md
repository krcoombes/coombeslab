---
author: Kevin R. Coombes
date: 2022-04-07
title: Coombes Lab
---

# Coombes Lab People

<table><tr>
<td class = "person"><h2> PI:  Kevin R. Coombes</h2></td>
<td class = "photo"><img src="images/kevin-coombes.jpg" width="200" alt="Kevin Coombes"></img></td>
</tr>

<tr><td><h2> Post-doctoral Fellows</h2></td><td></td></tr>

<tr>
<td class = "person"><h3>Jake Reed</h3>
2022-present. Undergraduate degree from Georgia Regents University (now known as Augusta University. Ph.D. from Cornell-Weill.</td>
<td class = "photo"><img src="images/jakereed.jpg" width="200" alt="Zach Abrams"></img></td>
</tr>

<tr>
<td class = "person"><h3>Zachary Abrams</h3>
2016-2021. Now an Instructor in the Department of Biostatistics and the Institute
for Informatics at Washington Univerity in St. Louis.</td>
<td class = "photo"><img src="images/zach-abrams.jpg" width="200" alt="Zach Abrams"></img></td>
</tr>

<tr>
<td class = "person"><h3>Amir Asiaee</h3>
2017-2021. Now an Assistant Professor in the Department of Biostatistics at
Vanderbilt University.</td>
<td class = "photo"><img src="images/amir-asiaee.jpg" width="200" alt="Amir Asiaee"></img></td>
</tr>

<tr>
<td class = "person"><h3>RB McGee</h3>
2015-2017. Now an Assistant Professor in the Department of Mathematics and
Computer Science at the College of the Holy Cross.</td>
<td><img src="images/mcgee.jpg" width="200" alt="RB McGee"></img></td>

<tr>
<td class = "person"><h3>Min Wang</h3>
2015-2017. Now a Lead Data Scientist at PayPal.</td>
<td class = "photo"><img src="images/min-wang.jpg" width="200" height="200" alt="Min Wang"></img></td>
</tr>

<tr><td><h2>Graduate Students</h2></td><td></td></tr>

<tr>
<td class = "person"><h3> Mark Zucker (PhD)</h3>
Ph.D., Ohio State University, 2018. Now a post-doctoral scholar at Memorial Sloan-Kettering.</td>
<td class = "photo"><img src="images/mark-zucker.jpg" width="200" alt="Mark Zucker"></img></td>
</tr>

 <tr>
<td class = "person"><h3> Pan Tong (PhD)</h3>
Ph.D., Univerity of Texas Graduate School of Biological Sciences, 2013. Now an Applied Scientist at Amazon.</td>
<td class = "photo"><img src="images/pan-tong.jpg" width="200" alt="Pan Tong"></img></td>
</tr>

<tr>
<td class = "person"><h3> Gayathri Warrier (MS)</h3>
M.S., 2017. Currently an Associate Specialist in Neurosurgery at UCSF.</td>

<td class = "photo"><img src="images/gayathri-warrier.jpg" width="200" alt="Gayathri Warrier"></img></td>
</tr>

<tr>
<td class = "person"><h3>Caitlin Coombes (MS)</h3>
M.S., 2020. Currently a medical student at OSU; soon to be a
anesthesiology resident at Stanford.</td>
<td class = "photo"><img src="images/cat-coombes.jpg" width="200"
height="200" alt="Caitlin Coombes"></img></td>
</td></tr>

<tr><td><h2>Undergraduates</h2></td><td></td></tr>

<tr>
<td class = "person"><h3> Anoushka Joglekar (2016-2017)</h3>
Now in a Ph.D. program in Computational Biology (in the Tilghner lab) at Weill-Cornell.</td>
<td class = "photo"><img src="images/joglekar.jpg" width="200" alt="Anoushka Joglekar"></img></td>
<tr>

<td class = "person"><h3> Suli Li</h3></td>
<td class = "photo"><img src="images/suli-li.jpg" width="200"
alt="Suli Li"></img></td>
</td></tr>

<tr>
<td class = "person"><h3> Tommy Bai</h3>
Now a Data Scientist somewhere in NY.</td>
<td class = "photo"><img src="images/tommy_bai.jpg" width="200"
alt="Tommy Bai"></img></td>
</td></tr>

<tr>
<td class = "person"><h3> Greg Gershkowitz</h3>
Now a Software Engineer at Microsoft.</td>
<td class = "photo"><img src="images/gershkowitz.jpg" width="200" alt="Geeg Gershkowitz"></img></td>
</tr>

<tr>
<td class = "person"><h3> Sam (Samantha) Nakayiza</h3></td>
<td class = "photo">
</td></tr>

<tr>
<td class = "person"><h3> Dwayne Tally</h3>
Now a Master's Student at Indiana State University.</td>
<td class = "photo">
</td></tr>

<tr>
<td class = "person"><h3> Philip Nicol</h3>
Now a Biostatistcs PhD student in the Michor Lab at Harvard.</td>
<td class = "photo"><img src="images/phillip-nicol.jpg" width="200"
alt="Phillip Nicol"></img></td>
</tr>

</td></tr></table>
