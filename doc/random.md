https://distill.pub/2016/misread-tsne/

This is the link we glanced at in your office. You might want to read the whole thing, if you haven't already. The clusters that Su Li and Zach have been putting out may need some tuning. Apparently narrow clusters can indicate you have your settings wrong.


Here is a review of recent developments in the structure learning of graphical models:
https://arxiv.org/pdf/1606.02359.pdf



https://www.tandfonline.com/doi/full/10.1080/00031305.2017.1375989?scroll=top&needAccess=true


http://chat.lionproject.net/help  
