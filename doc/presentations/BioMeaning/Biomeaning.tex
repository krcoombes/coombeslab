\documentclass[t]{beamer}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{sansmathaccent}
\usepackage{ulem} % for strikethrough
\pdfmapfile{+sansmathaccent.map}
\definecolor{krc}{rgb}{0.521, 0.118, 0.369} % 1C    3D   {0.3, 0.3, 0.9}
\definecolor{osuorange}{rgb}{0.8235,0.3725,0.0824}
\definecolor{osupurple}{rgb}{0.65,0.05,0.55}
\newcommand{\krc}{\vskip0pt plus 1filll}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up the meta-information for the PDF file
\hypersetup{
  pdftitle={Biological Meaning From Mathematical Models},
  pdfsubject={transcription factors},
  pdfauthor={Kevin R.~Coombes,
    Department of Biomedical Informatics,
    The Ohio State University,
    <coombes.3@osu.edu>
  },
  pdfkeywords={Thresher,hierarchical clustering,principal components analysis,outliers},
  pdfpagemode={None},
  pdfpagetransition=Replace,
  colorlinks,
  linkcolor=,%internal, deliberately left blank
  urlcolor=krc%external
  }

\usetheme[secheader]{Columbus}
% color the Sinput and Soutput chunks
\setbeamerfont{block body}{size=\small}
\usepackage[nogin]{Sweave}
\DefineVerbatimEnvironment{Sinput}{Verbatim} {xleftmargin=1em}
\DefineVerbatimEnvironment{Soutput}{Verbatim}{xleftmargin=1em}
\DefineVerbatimEnvironment{Scode}{Verbatim}{xleftmargin=1em}
\fvset{listparameters={\setlength{\topsep}{0pt}}}
\newenvironment<>{Schunk}{%
  \begin{actionenv}#1%
    \parskip=0pt\vspace{\topsep}
    \def\insertblocktitle{}%
    \par\vspace{\topsep}%
    \usebeamertemplate{block begin}}
  {\par\parskip=0pt\vspace{\topsep}%
    \usebeamertemplate{block end}%
  \end{actionenv}}

\mode
<presentation>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%\setkeys{Gin}{width=\textwidth}

%%%%%%%%%%%%%%%%%%%%
% need the usual title, author, etc for splash page as well as title page
\title[Biology from Math Models]{Biological Meaning From Mathematical Models} 
\author[\copyright\ Copyright 2019, Kevin R.~Coombes]{Kevin R. Coombes\\
  \textcolor{pms7532}{\tt coombes.3@osu.edu}
}
\institute{Department of Biomedical Informatics\\
  The Ohio State University
}
\titlegraphic{
  \includegraphics[width=3in]{./Figures/OSU-WexMedCtr-4C-Horiz-CMYK}
}
\date{6 December 2019}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% actually start creating frames
\begin{frame}[plain,noframenumbering]
  \titlepage
\end{frame}

\parskip=5pt plus3pt minus2pt  % probably belongs in a 'theme' style somewhere.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\subsection{Pathways}
\begin{frame}
  \frametitle{Motivation}

\begin{multicols}{2}

\textcolor{osuscarlet}{\textbf{Biological Question}: How do we
  understand pathways?}

Biologists talk about (signaling) pathways in a cell as though they
are on-off switches. In a population of cells, that means you should
be able to represent the state of the pathway by a single number: the
fraction of cells where the pathway is active. In particular, it would
be one-dimensional.

  \includegraphics[width=60mm]{./Figures/on-off-switch}
\end{multicols}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Apoptosis: This is not an on-off switch}
  \includegraphics[width=120mm]{./Figures/apop00}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Better Questions}

\textcolor{osuscarlet}{\textbf{Question}: What is the ``dimension'' of a pathway?}

Mathematically more precise version of the question:

Given
\begin{itemize}
\item an actual biological pathway where nodes = genes, and
\item a heap of data measured at those genes in some samples.
\end{itemize}
Find
\begin{itemize}
\item the intrinsic dimension of that set of data.
\end{itemize}

The use of the term ``\textcolor{osuscarlet}{intrinsic}'' is
deliberate. It suggests that the answer should be the same for
different data sets, provided they have enough data to explore all of
the biologically possible states of the system.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Linear Algebra}
\begin{frame}
\frametitle{Singular Value Decomposition}
\begin{multicols}{2}

\begin{itemize}
\item Let $X$ be any real matrix with $m$ rows and $n$ columns.
\item There is a unique ``singular value'' decomposition
  $$X = V^TDU$$
  where
  \begin{enumerate}
  \item $U$ is an $n\times n$ unitary matrix (so $U^TU=I_n$),
  \item $V$ is an $m\times m$ unitary matrix (so $V^TV=I_m$), and
  \item $D$ is a diagonal matrix with nonincreasing nonnegative
    entries.
  \end{enumerate}
\end{itemize}

\textcolor{osuscarlet}{\textbf{Consequences}}

The singular value decomposition of the transpose $X^T$ is
$$X^T = U^TDV.$$

The covariance matrix of $X$ is
\begin{align*}
  X^TX &= (V^TDU)(U^TDV)\\
  &= V^TD(U^TU)DV\\
  &= V^TD^2V.
\end{align*}

Diagonal elements of $D^2$ are the variances of each feature.
\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Principal Components Analysis}

Simulated Data (Two Binary Factors in $\mathbb{R}^{13}$)

~\includegraphics[width=56mm]{Figures/spca}
~\ \includegraphics[width=56mm]{Figures/scree}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{PCDimension}
\subsection{N Components}
\begin{frame}
\frametitle{Number of Significant Components}
Reference: \textcolor{osuorange}{Auer P, Gervini D. Choosing
principal components: a new graphical method based on Bayesian model
selection. Commun Stat Simulat, 2008; 37: 962--977.}

View the problem as one of model selection, where the model $M_D$ says
that there are $D$ components. Mathematically, says that eigenvalues
in the PC matrix satisfy
$$ \lambda_1 \ge \lambda_2 \ge \cdots \ge \lambda_D,
 \qquad \lambda_D > \lambda_{D+1},
 \qquad \lambda_{D+1} = \lambda_{D+2} = \cdots = \lambda_M $$

After specifying a prior distribution on $D$, they compute the
posterior probability of each model, and choose the model with the
maximum posterior probability.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Auer-Gervini Priors}
Use a family of prior distributions on the number $D$ of components.
$$ P(D) \propto exp(-\theta n D/ 2) $$
As $\theta \mapsto 0$, prior becomes flat: choose maximum possible
number of components. As $\theta \mapsto \infty$, prior increasingly
forces choice of zero components.

\begin{center}
  \includegraphics[width=106mm]{./Figures/ag-priors}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Evaluating All the Priors}
\begin{multicols}{2}
  \includegraphics[width=53mm]{./Figures/auergervini}

Auer \& Gervini plot the number $D$ of components as a function of the
prior parameter $\theta$.  This is a step function.  Longer steps mean
 more people would choose that value of $D$.
 \textcolor{osuscarlet}{Pick the ``highest step that is a reasonable length''.}

Our additions: [1] \textcolor{osuscarlet}{Define largest sensible
  $\theta$ as one that assigns $99\%$ probability to $D=0$.}  [2]
\textcolor{osuscarlet}{Test different definitions of ``reasonable''.}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{An Objective ``Reasonable'' Step Length}

Reference: \textcolor{osuorange}{Wang M, Kornblau SM, Coombes
  KR. Decomposing the Apoptosis Pathway Into Biologically
  Interpretable Principal Components. \textit{Cancer
    Informatics}. 2018;17:1176935118771082.}

\texttt{PCDimension} package available on CRAN.

\begin{itemize}
\item Idea is to separate the step lengths into two groups: ``small''
  and ``significantly large''.
\item Developed an R package that implemented and tested eight
  criteria for separation:
    \begin{enumerate}   \itemsep2pt
    \item[\textcolor{black}{1--4:}] TwiceMean, Kmeans, Kmeans3 and Spectral.
    \item[\textcolor{black}{5--8:}] Ttest, Ttest2, CPT and CpmFun.
    \end{enumerate}
\item Compared with ``best'' methods from earlier studies.
\item Performed simulations with various data sizes and correlation
  structures.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Simulation Results}
\begin{center}
\includegraphics[height=60mm]{./Figures/fig2(lwd=3_error_0).jpeg} 
\end{center}
\textcolor{osuscarlet}{Auer-Gervini} (\textcolor{pink}{CPT} or
\textcolor{magenta}{TwiceMean}) and 
\textcolor{green}{rnd-Lambda} are the best on average.  Auer-Gervini
is two orders of magnitude faster.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{AML RPPA Data}
\begin{frame}
\frametitle{AML RPPA Data}

\begin{center}
  \includegraphics[width=105mm]{Figures/RPPA-Apop/CASP3clvd}
\end{center}
\begin{itemize}\parskip=0pt\itemsep=2pt
\item RPPA = Reverse Phase Protein (lysate) Array
  \begin{itemize}
  \item Micro dot blots; samples printed (Aushon 2470 arrayer) in a
    dilution series on nitrocellulose membranes fixed to glass
    microscope slides.
  \item Antibody-based detection ($> 200$ antibodies)
  \item Slides are scanned, spots quantified (using MicroVigene), then
    dilution series quantified (using SuperCurve) and normalized.
  \end{itemize}
\item Collaboration with Steve Kornblau at M.D. Anderson
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{AML RPPA Data: Sample Summary}

\begin{itemize}
\item 511 newly diagnosed cases of AML
  \begin{itemize}
  \item $57\%$ Male, $43\%$ Female
  \item Median age: $64.9$ years (range: $16$--$87$)
  \item Cytogenetics: $7\%$ favorable, $44\%$ Intermediate, $49\%$ Unfavorable
  \item $17\%$ FLT3-ITD$+$
  \item Prior malignancy ($24\%$), chemo ($11\%$), xrt ($9\%$)
  \item $40\%$ with AHD $\ge2$ months
  \end{itemize}
\item Expression controls
  \begin{itemize}
  \item Eleven CD34+ normal bone marrow
  \item Ten G-CSF primed CD34+ normal marrow
  \item Ten Normal peripheral blood lymphocytes
  \item Four cell lines (Jurkat $\pm$ Fas; Daudi $\pm$ IL4)
  \end{itemize}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Apoptosis-Related Proteins in RPPA Data Set}

\begin{Schunk}
\begin{Soutput}
 [1] "AIFM1"       "ARC"         "BAD"         "BAD.pS112"  
 [5] "BAD.pS136"   "BAD.pS155"   "BAK1"        "BAX"        
 [9] "BCL2"        "BCL2L1"      "BCL2L11"     "BID"        
[13] "BIRC2"       "BIRC5"       "BMI1"        "CASP3"      
[17] "CASP3.cl175" "CASP7.cl198" "CASP8"       "CASP9"      
[21] "CASP9.cl315" "CASP9.cl330" "MCL1"        "MDM2"       
[25] "MDM4"        "PARP1"       "PARP1.cl214" "YAP1"       
[29] "YAP1p"       "DIABLO"      "TP53"        "TP53.pS15"  
[33] "XIAP"       
\end{Soutput}
\end{Schunk}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Apoptosis is $\ge$ Six Dimensional (RPPA)}

~\includegraphics[width=56mm]{Figures/RPPA-Apop/apoptosis-01-scree}
~\includegraphics[width=56mm]{Figures/RPPA-Apop/apoptosis-02-bayes}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{TCGA Transcriptome Data}
\begin{frame}
\frametitle{TCGA RNA-Seq Data}

\textcolor{osuscarlet}{Get the data:}

\begin{itemize}
\item Download ``RSEM\_genes\_normalized'' TCGA RNA-sequencing data
  from  \url{http://firebrowse.org}
\item 33 cancer types
\item 10,446 samples
  \begin{itemize}
  \item 9,326 primary tumor
  \item 395 metastases
  \item 725 adjacent normal
  \end{itemize}
\item Number of samples per cancer type ranges between
  \begin{itemize}
  \item 45 (CHOL; cholangiocarcinoma)
  \item 1212 (BRCA; breast invasive carcinoma).
  \end{itemize}
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Apoptosis is Six or Twelve Dimensional (TCGA)}

~\includegraphics[width=56mm]{Figures/TCGA-Apop/tcga-scree}
~\includegraphics[width=56mm]{Figures/TCGA-Apop/tcga-ag}

But ``dimension'' is still a mathematical concept. Where is the biology?
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Thresher}
\subsection{Biological Components}
\begin{frame}
\frametitle{Biological Components (Not Principal)}

\begin{multicols}{2}

Simulated Data Revisited

\includegraphics[width=56mm]{Figures/arrows}

\begin{enumerate}
\item Many of the simulated features are irrelevant to the
  clustering. Can \textcolor{osuscarlet}{remove outliers based on the
    length} or ``weight'' of the features on the set of significant
  principal components.
\item Three real features but in only two dimensions. Want to
  \textcolor{osuscarlet}{cluster based on the directions} of PC
  vectors.
\end{enumerate}

\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Thresher}

Reference: \textcolor{osuorange}{Wang M, Abrams ZB, Kornblau SM,
  Coombes KR. Thresher: Determining the Number of Clusters while
  Removing Outliers. \textit{BMC Bioinformatics}. 2018; 19(1):9.}

We built another R package, \texttt{Thresher}, that combines
Auer-Gervini to determine the PC dimension, outlier removal, and
feature clustering using mixtures of von Mises-Fisher distributions.

The package is available on CRAN.

\textcolor{osuscarlet}{Idea:} View the genes you want to cluster as
``features'' that produce ``weight vectors'' based on their
contributions to significant components. Outliers have short
length. Then cluster everything that remains based on their directions
in PC space.

\textcolor{blue}{Real Motivation: Sacrifices orthogonality for
a chance at getting biologically interpretable clusters.}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Clustering on Hyperspheres}
\begin{frame}
\frametitle{Clustering on Hyperspheres}
\begin{center}
\includegraphics[width=80mm]{./Figures/vmf.png}
\end{center}

{\small\centering

Figure 1, Hornik and Gr\"un.

}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Clustering on Hyperspheres}
I'll spare you the details.  Key points are [1] Someone has already
figured out which distribution to use:

Reference: \textcolor{osuorange}{Banerjee A, Dhillon IS, Ghosh J, Sra
  S. Clustering on the unit hypersphere using von Mises-Fisher
  distributions. J Mach Learn Res, 2005; 6: 1345--1382.}

and [2] someone else has already implemented this mixture model in an
R package:

Reference: \textcolor{osuorange}{Hornik K, Gr\"un B. movMF: An R
  package for fitting mixtures of von Mises-Fisher distributions. J
  Stat Software, 2014; 58(10): 1--31.}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}
\subsection{Extensive Simulations}
\begin{frame}
\frametitle{Accuracy in Estimating the Number of Clusters}
\begin{center}
  \includegraphics[width=104mm]{Figures/fig2}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Apoptosis (RPPA)}
\begin{frame}
\frametitle{Results for Apoptosis (RPPA)}
  \includegraphics[width=36mm]{Figures/RPPA-Apop/apoptosis-cleaned-04A-loadings}
  \includegraphics[width=36mm]{Figures/RPPA-Apop/apoptosis-cleaned-04B-loadings}
  \includegraphics[width=36mm]{Figures/RPPA-Apop/apoptosis-cleaned-04C-loadings}

  \begin{enumerate}
  \item (red) AIFM1, BCL2, DIABLO
  \item (blue) ARC, BAD.pS112, YAP1p
  \item (green) BAD.pS136, BAD.pS155, BAX
  \item (purple) BID, CASP3, CASP8, XIAP
  \item (orange) CASP3.cl175, CASP7.cl198, CASP9.cl315, MDM2
  \item (gray) Everything else...
  \end{enumerate}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Apoptosis (TCGA)}
\begin{frame}
\frametitle{Results for Apoptosis (TCGA)}

  \includegraphics[width=120mm]{./Figures/apop2}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{IL4 Signaling (TCGA)}
\begin{frame}
\frametitle{Results for IL4 Signaling (TCGA)}

\hbox to \textwidth{Normal Lung \hfil LUSC \hfil LUAD \hfil}

  \includegraphics[width=36mm]{Figures/IL4/NORMAL_IL4_dense}
  \includegraphics[width=36mm]{Figures/IL4/LUSC_IL4_dense}
  \includegraphics[width=36mm]{Figures/IL4/LUAD_IL4_dense}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Regulatory Molecules}
\subsection{More Questions}
\begin{frame}
\frametitle{Good Answers Lead to More Questions}

We now have a tool that decomposes a biological data set into
one-dimensional components that can (potentially) be interpeted
biologically. What else can we use it for?

\textcolor{osuscarlet}{Question:} How much of (mRNA) gene expression
can be explained by the (mRNA) expression of transcription factors?

\textcolor{osuscarlet}{Question:} How much of (mRNA) gene expression
can be explained by the expression of microRNAs (miRs)?

\textcolor{osuscarlet}{Idea:} Use the pan-cancer sequencing data
from The Cancer Genome Atlas (TCGA) to try to answer these question.

Note that the full data set consists of gene expression measurements
on about 10,000 samples.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Transcription Factors}

\textcolor{osuscarlet}{Get a list of transcription factors:}

\begin{itemize}
\item Go to the \textbf{TFCat Transcription Factor Database}${}^*$
  (\sout{\url{http://www.tfcat.ca/}} Nov 2019: \url{http://cisreg.ca/tfcat/}).
\item Search for ``TF Gene'' and download everything.
\item Database uses \textbf{mouse} Entrez Gene IDs as their
  primary identifier.
\item Convert gene symbols to upper case and look up the
  corresponding gene in humans.
\item Only retain genes that were manually curated with
  ``\textbf{strong}'' evidence of being transcription factors.
\item Final list contains 486 transcription factors.
\end{itemize}

{\small

\textcolor{osuorange}{${}^*$Fulton DL, Sundararajan S, Badis G, Hughes
  TR, Wasserman WW, Roach JC, Sladek R. TFCat: the curated catalog of
  mouse and human transcription factors. Genome Biology 2009; 10:R29.}

}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Color Schemes}

Reference: \textcolor{osuorange}{Coombes KR, Brock G, Abrams ZB,
  Abruzzo LV, Polychrome: Creating and Assessing Qualitative Palettes
  With Many Colors. \textit{J Statist Software}. July 2019, Volume 90,
  Code Snippet 1.} Available on CRAN.

\begin{center}
  \includegraphics[height=42mm]{Figures/legend}
  \includegraphics[height=42mm]{Figures/scheme-1}
\end{center}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{t-SNE plots}
\begin{frame}
\frametitle{Transcription Factors Can Distinguish Cancer Types}

\noindent
\begin{minipage}[c]{0.65\linewidth}
\begin{center}
  \includegraphics[width=72mm]{Figures/tf-tsne}
\end{center}
\end{minipage}
\begin{minipage}[c]{0.3\linewidth}
  \raggedright\small
  \textcolor{osuorange}{Abrams ZB, Zucker M, Wang M, Asiaee Taheri A,
  Abruzzo LV, Coombes KR. Thirty biologically interpretable clusters
  of transcription factors distinguish cancer type. \textit{BMC
    Genomics}. 2018 Oct 11;19(1):738.} 
\end{minipage}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{MicroRNAs Can Distinguish Cancer Types}

\noindent
\begin{minipage}[c]{0.65\linewidth}
\begin{center}
  \includegraphics[width=72mm]{Figures/Asiaee_Fig_2}
\end{center}
\end{minipage}
\begin{minipage}[c]{0.3\linewidth}
  \raggedright\small
  \textcolor{osuorange}{Asiaee A, Abrams ZB, Nakayiza S, Sampath D,
    Coombes KR. Explaining Gene Expression Using Twenty-One
    MicroRNAs. \textit{J Comput Biol}. 2019 Dec 2. doi:
    10.1089/cmb.2019.0321. [Epub ahead of print]} 
\end{minipage}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Interpretation}
\subsection{Transcription Factors}
\begin{frame}
  \frametitle{Thresher for Transcription Factors}
\begin{multicols}{2}
\begin{center}
  \includegraphics[width=56mm]{Figures/TF/ag-1}
\end{center}
\begin{itemize}
\item Found 30 clusters, which we are currently calling ``biological
  components''.
\item Looked at each transcription factor in UniGene to get a list of
  tissues where it is known to be expressed.
\item Recorded the ``most commonly seen tissue'' for the transcription
  factores in each cluster.
\item Also averaged the expression of transcription factors in each
  cluster to get ``biological scores'' for each sample.
\end{itemize}
\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Components (Tissue Specific)}
  \includegraphics[width=120mm]{Figures/TF/sorted-BC4}
  \includegraphics[width=58mm]{Figures/TF/sorted-BC2}
  \includegraphics[width=58mm]{Figures/TF/sorted-BC9}
  \includegraphics[width=58mm]{Figures/TF/sorted-BC17}
  \includegraphics[width=58mm]{Figures/TF/sorted-BC26}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Components (Embryonically Lethal)}
  \includegraphics[width=120mm]{Figures/TF/sorted-BC1}
  \includegraphics[width=120mm]{Figures/TF/sorted-BC29}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{MicroRNAs}
\begin{frame}
  \frametitle{Thresher for MicroRNAs}
\begin{multicols}{2}
\begin{center}
  \includegraphics[width=56mm]{Figures/MIR/mir-ag}
\end{center}
\begin{itemize}
\item Worked with 8895 patient samples that had both mRNA and miR
  sequencing in TCGA.
\item Filtered the miR data set to keep only 470  miRs that were
  present in 25\% of samples.
\item Found 21 clusters, or ``biological components''.
\item  Performed enrichment analysis using miRs enrichemnt and
  annotation(miEAA) tool.
\item Also used gene enrichment for sets of genes that were correlated
  with the average of each miR-cluster.
\end{itemize}
\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Biological Components (Tissue Specific)}
  \includegraphics[width=120mm]{Figures/MIR/sorted-MIRC5}
  \includegraphics[width=58mm]{Figures/MIR/sorted-MIRC15}
  \includegraphics[width=58mm]{Figures/MIR/sorted-MIRC17}
  \includegraphics[width=58mm]{Figures/MIR/sorted-MIRC18}
  \includegraphics[width=58mm]{Figures/MIR/sorted-MIRC20}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Modeling Expression}
\subsection{Basics}
\begin{frame}
\frametitle{Modeling Expression}

Now we can retun to our starting questions: \textcolor{osuscarlet}{To
  what extent does the mRNA expression of transcription factors or
  microRNAs explain the mRNA expression of other genes?}

We obviously won't use all 486 transcription factors.  Instead, we'll
use the 30 biological components that we have derived from our
clustering analysis.  So, for each gene, we want to fit a model that
looks like:

\begin{block}{}
  \begin{center}
    Y $\sim$ BC1 + BC2 + BC3 + $\dots$ + BC30.
  \end{center}
\end{block}

We fit the model with half the data and used it to make predictions
on the other half.

Reference: \textcolor{osuorange}{Asiaee A, Abrams ZB, Nakayiza S, Sampath D,
  Coombes KR. Identification and comparison of genes differentially
  regulated by transcription factors and miRNAs, \textit{bioRxiv
    preprent}. doi: \url{https://doi.org/10.1101/803643}}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{P Values}
\begin{frame}
\frametitle{Why P-Values Are Worthless}

$99.8\%$ of the p-values are less than $0.01$. (There are 5000 samples.) 
\begin{center}
  \includegraphics[width=96mm]{Figures/LinMod/bum-1}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Coefficient of Determination}
\begin{frame}
\frametitle{TF - Percent of Variance Explained}

\begin{center}
  \includegraphics[width=96mm]{Figures/LinMod/tf-only-r2}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{MIR - Percent of Variance Explained}

\begin{center}
  \includegraphics[width=96mm]{Figures/LinMod/mir-only-r2}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{TF and MIR - Percent of Variance Explained}

\begin{center}
  \includegraphics[width=96mm]{Figures/LinMod/tfmir-r2}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
\item \texttt{Polychrome} produces useful palettes with lots of colors.
\item \texttt{PCDimension} (using the Twice mean criterion to automate
  the Auer-Gervini algorithm) seems like the best method to determine
  the number of principal components.
\item \texttt{Thresher} produces biologically interpretable
  non-principal components (BCs).
\item Using TFs and miRs and a linear model with 51 factors can
  explain , on average, about 65\% of variation across the entire
  transcriptome. 
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Acknowledgement}
 \begin{itemize} 
  \item Dr. Min Wang, The Ohio State University.
  \item Dr. Zachary Abrams, The Ohio State University.
  \item Dr. Amir Asiaee, The Ohio State University.
  \item Dr. Mark Zucker, Memorial Sloan-Kettering.
  \item{}
  \item Dr. Lynne V. Abruzzo, The Ohio State University.
  \item Dr. Guy Brock, The Ohio State University.
  \item Dr. Deepa Sampath, The Ohio State University.
  \item Dr. Steven Kornblau, The University of Texas MD Anderson Cancer
Center.
  \item[]
  \item Grants: NSF No. 1440386; NIH/NCI P30 CA016058; R01 CA182905,
    NIH/NLM T15LM011270.
 \end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TOC at end ...
\begin{frame}[plain,noframenumbering]
\begin{multicols}{2}
  \tableofcontents
\end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
