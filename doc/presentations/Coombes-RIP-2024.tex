\documentclass[t,svgnames,aspectratio=169]{beamer}
\usepackage{xcolor}
\usepackage{multicol}
\usepackage{sansmathaccent}
\pdfmapfile{+sansmathaccent.map}
\definecolor{krc}{rgb}{0.521, 0.118, 0.369} % 1C    3D   {0.3, 0.3, 0.9}
\hypersetup{
  pdftitle={Building Biology-Based Bioinformatics Tools},
  pdfsubject={R packages},
  pdfauthor={Kevin R. Coombes,
    Department of Biostatistics, Data Science, and Epidemiology,
    Georgia Cancer Center at Augusta University,
    <kcoombes@augusta.edu>
  },
  pdfkeywords={visualization,clustering,classification,dimension},
  pdfpagemode={UseNone},
  colorlinks,
  linkcolor=,%internal, deliberately left blank
  urlcolor=accent1color%external
  }
\usetheme[secheader]{Augusta}
% [gradient] puts a gradient fill one the background of everything
% except the splash page
% [secheader] puts title, section, and subsection in the headline

\mode
<presentation>

\begin{document}

%\setkeys{Gin}{width=154mm} % beamer 16x9 claims to be 170.67mm by 96mm

%%%%%%%%%%%%%%%%%%%%
% need the usual title, author, etc for splash page as well as title page
\title{Building Biology-Based Bioinformatics Tools}
\author[\copyright\ Copyright 2024, Kevin R.~Coombes]{Kevin R. Coombes\\
  \textcolor{Linkcolor}{\tt kcoombes@augusta.edu}
}
\institute{Department of Biostatistics, Data Science, and Epidemiology\\
  Georgia Cancer Center at Augusta University
}
\titlegraphic{
  \includegraphics[height=12mm]{Figures/au-cancercenter}
}
\date{17 January 2024}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% actually start creating frames
\begin{frame}[plain,noframenumbering]
  \titlepage
\end{frame}

\parskip=5pt plus3pt minus2pt  % probably belongs in a 'theme' style somewhere.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overview}
\begin{frame}
  \frametitle{Coombes Lab R Packages}
  \begin{center}
    \includegraphics[width=96mm]{Figures/BBBB-overview}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data Visualization}
\subsection{Motivating Question}
\begin{frame}
  \frametitle{Viewing Genome-Scale Data}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item \textcolor{accent2color}{When analyzing omics technologies, can you display data in a
        way that indicates which chromosome each item comes from?}
      \item {In other words, can you find 24 different colors that most
        people can distinguish in a plot?}
      \item {Has anyone done this previously? If so, can we just steal the
        palette from them?}
      \item {Or, can we do better?}
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        (Nearly) Normal Spectral Karyotype

        \includegraphics[height=40mm]{Figures/spectral-normal}

        {\small

          Anguiano, Molecular Cytogenetics, 2012;5(3)
        }
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Spectral Karyotyping (SKY)}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
       Reticulum Cell Sarcoma of Lymph Node

       \includegraphics[height=40mm]{Figures/lymph-node-sarcoma}

       {\small

         Jones, Modern Pathology 2001;14(10)
       }
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
       Breast Cancer Cell Line OCUB-F

       \includegraphics[height=40mm]{Figures/spectral-cellLine}

       {\small

         Davidson, British Journal of Cancer 2000;83(10)
       }
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Polychrome}
\begin{frame}
  \frametitle{Polychrome R Package}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[height=32mm]{Figures/polychrome}
      \end{center}
      \begin{itemize}
        \item Contains tools to create and visualize palettes of
          distinguishable colors.
        \item Includes ``color-safe'' palettes that should be
          distinguishable by people with different forms of color
          vision deficits.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Need to consider various color spaces: RGB, CMYK, HSV,
        HSL.
        \item Key is L*u*v*, where L is luminance and the pair (u,v)
          defines colors.
        \item \textcolor{accent2color}{(Carter \& Carter, 1981, 1982)
          experimentally showed that:}
          \begin{itemize}
            \item \textcolor{accent2color}{Ability to distinguish
              colors is monotonically related to Euclidean distance in
              L*u*v* space.}
            \item \textcolor{accent2color}{Minimum distance at which
              colors can be distinguished is about 40 units.}
          \end{itemize}
        \item Most previous discrete palettes did not test this
          criterion for their colors.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Application}
\begin{frame}
  \frametitle{Improved SKY Colors}
  \begin{center}
    \includegraphics[height=66mm]{Figures/sky}
  \end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Seurat UMAP Plots}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        Data from Lynn Hedrick's lab
        \includegraphics[height=56mm]{Figures/seurat1}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        Same data, recolored
        \includegraphics[height=56mm]{Figures/seurat2}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dimension Reduction}
\subsection{Mercator}
\begin{frame}
  \frametitle{Mercator R Package}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[height=32mm]{Figures/merc-paper}
      \end{center}
      \begin{itemize}
      \item Contains tools to visualize clustered data using
        different dimension reduction tools.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Supported Visualizations:
        \begin{itemize}
        \item Hierarchical Clustering
        \item Multi-Dimensional Scaling
        \item t-Stochastic Neighbor Embedding
        \item Uniform Manifold Approximation and Projection
        \item Self-Organizing Maps
        \item Graphs
        \item Silhouette Widths
        \end{itemize}
      \item Also makes it easy to find ``best match'' clusters across
        different algorithms.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Application}
\begin{frame}
  \frametitle{Mercator Views of Simulated Truth}
  \begin{center}
    \includegraphics[height=66mm]{Figures/mercator}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Same Simulated Data, Hierarchical Clustering}
  \begin{center}
    \includegraphics[height=66mm]{Figures/mercator-hc}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{A Shattered Sphere}
  \begin{center}
    \includegraphics[height=66mm]{Figures/sphere}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sequence Alignment}
\subsection{Motivation}
\begin{frame}
  \frametitle{HPV-positive Head and Neck Cancer}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=56mm]{Figures/gillison2019}

        \begin{itemize}
          \item \textcolor{accent2color}{Are copies of the virus in
            HPV+ HNSCC integrated into the human genome or present as
            extrachromosomal DNA?}
          \item My collaborators (Maura Gillison and David Symer)
            believed all were integrated.
          \item Other researchers claimed all were extrachromosomal.
        \end{itemize}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \begin{itemize}
        \item Collected ``standard'' NGS along with TenX ``long read'' sequences.
        \item Bayesian model viewing the long read data as 3 possible
          kinds of molecules.
        \item \textcolor{accent2color}{Everything turns out to be a mixture.}
        \end{itemize}
        \includegraphics[width=44mm]{Figures/tenX-033}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Complex Structural Variants}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=60mm]{Figures/symer2022}
      \end{center}
      \begin{itemize}
      \item Moved on from TenX to Oxford Nanopore
        Technologies (ONT) long-read sequencing.
      \item \textcolor{accent2color}{Can we disentangle
        structural variants involving viral and human DNA?}
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width = 54mm]{Figures/1221_evolution_schematics}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Heterocateny}
\begin{frame}
  \frametitle{Heterocateny}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=60mm]{Figures/akagi2023}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Heterocateny, n. ``Variable chains''.
      \item \url{https://www.youtube.com/watch?v=h2Fyo3cj5GQ}
      \item Idea is that there is a sequence of interactions between
        the viral DNA and human DNA, including
        \begin{itemize}
        \item Insertion into a human chromosome.
        \item Extraction from the chromosome, bringing material along.
        \item Circularization as extrachomosonal DNA (episomes).
        \item Duplication of circular elements.
        \item Deletions of extrachromosomal material.
        \end{itemize}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Sample GS1221}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=60mm]{Figures/hg37HPV16-GS_1221.1000.png}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item NGS identified 95 breakpoints involving the virus and 4
        human chromosomes
        \begin{itemize}
        \item 13 between chr2 and HPV
        \item 6 between chr12 and HPV
        \item 2 between chr6 and HPV
        \item 1 between chr3 and HPV
        \item 11 between two human chromosomes
        \end{itemize}
      \item ONT produced 1499 long read sequences that crossed a
        breakpoint (LRBPS). Of these, 761 are distinct.
      \item Coded breakpoint with unique symbols; wrote LRBPS as a
        \textcolor{accent2color}{short} sequence of breakpoints.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{SVAlignR}
\begin{frame}
  \frametitle{SVAlignR R Package}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        De Bruijn Graph (M = 1)
        \includegraphics[width=80mm]{Figures/bp61-fig19-1}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        Manual Reconstruction of Initial Events
        \includegraphics[width=60mm]{Figures/cat1}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{SVAlignR R Package}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        De Bruijn Graph (M = 6)
        \includegraphics[width=70mm]{Figures/bp61-fig03-1}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        Manual Reconstruction of Loop
        \includegraphics[width=60mm]{Figures/bp61-fig05-1}

        \includegraphics[width=50mm]{Figures/cat2}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Integration into Chromosome 12}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=64mm]{Figures/bp61-fig16-1}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=56mm]{Figures/cat12}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulations}
\subsection{UMPIRE}
\begin{frame}
  \frametitle{Umpire R Package}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=64mm]{Figures/umpire2012}

        \includegraphics[width=64mm]{Figures/umpire2021}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Multi-hit theory of cancer
      \item A hit is a latent factor that modifies:
        \begin{itemize}
        \item ``Pathways'' = Gene expression in a correlated block of
          genes (modeled by a multivariate Gaussian).
        \item Survival (modeled by Cox regression).
        \item A binary outcome (modeled by logistic regression).
        \end{itemize}
      \item Combinatorial number of hits (example: 5 out of 20 possible)
      \item Model subtypes by taking different (possibly overlapping)
        subsets of hits.
      \item Add an extra layer of ``technical'' noise.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Mercator Views of Simulated Truth}
  \begin{center}
    5/20 possible hits; 12 hit combinations; 1000 samples; 10000
    features; medium noise

    \includegraphics[height=66mm]{Figures/mercator}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Clustering}
\subsection{Motivation}
\begin{frame}
  \frametitle{Which Clustering Algorithms Are Best?}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \textcolor{accent2color}{Reasonably Good Existing Algorithms:
        (Rodriquez, PLoS ONE, 2019:14;e0210236)}
      \begin{itemize}
      \item Hierarchical Clustering (1963)
      \item K-Means (1967)
      \item Partitioning Around Medoids (PAM; 1980's)
      \item Clustering Large Applications (CLARA; 1980's)
      \item Subspace Clustering (1998)
      \item Spectral Clustering (2001)
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \textcolor{accent2color}{Simulated Data Set Parameters for Umpire:}
      \begin{itemize}
      \item Number of Clusters: 3, 6, 12
      \item Number of Samples: 600, 1000
      \item Number of Features: 5000, 10000
      \item Noise Level: Low, Medium, High
      \end{itemize}

      \textcolor{accent2color}{Evaluation Criteria:}
      \begin{itemize}
      \item Adjusted Rand Index (external)
      \item Entropy (external)
      \item Mean Silhouette Width (internal)
      \item Normalized Within-Group Sum of Squared Errors (internal)
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Silhouette Width}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Given $x_i \in X$, an element in a clustered data set.
      \item Let $a(x_i)$ be the average distance from $x_i$ to all the
        elements in its assigned cluster.
      \item Let $b(x_i)$ be the minimum distance from $x_i$ to any of
        the other clusters.
      \item Define the silhouette width (SW) as:
        $$s(x_i) = \frac{b(x_i) - a(x_i)}{\min(b(x_i), a(x_i))}.$$
      \item Now $-1 \le s(x_i) \le 1$. Negative is poorly
        clustered; positive is well clustered.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Historically, the mean silhouette width (ASW) was used to
        decide which of two sets of clusters assignments was better.
      \item ASW is an ''internal'' measure of cluster quality, since
        it doesn't compare to an external measure of ground truth.
      \item But SW gives information about how well each individual item
        has been clustered.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{SillyPutty}
\begin{frame}
  \frametitle{SillyPutty Algorithm}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=80mm]{Figures/npr-sillyPutty}
        TomCopeland/AP/npr.org
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=72mm]{Figures/algo}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{SillyPutty In Action}
  \vskip-10mm
  \begin{center}
    \includegraphics[width=64mm]{Figures/threegp}
    \vskip-5mm
    \url{./ANIME/01-sillyAlg.html}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{SillyPutty Performance}
  \begin{center}
    \includegraphics[width=148mm]{Figures/OverallSW}
  \end{center}

  \textcolor{accent2color}{Note:} Because SillyPutty can take any
  initial cluster assignments (not just random), we can apply it after
  any other clustering algorithm.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{SillyPutty Performance}
  \begin{center}
    \includegraphics[width=148mm]{Figures/OverallARI}
  \end{center}

  \textcolor{accent2color}{Adjusted Rand Index} is an external measure
    of performance; it measures how well the cluster assignments match
    known truth.
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Hierarchical + SillyPutty is Best and Fastest
    Clustering Algorithm}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \includegraphics[height=52mm]{Figures/perftable}
    \end{column}
    \begin{column}{0.5\textwidth}
      \includegraphics[height=52mm]{Figures/timetable}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dimension}
\subsection{Motivation}
\begin{frame}
  \frametitle{Kinds of Dimensions}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item An omics data set puts samples or cells into a very high
        dimensional (at least tens and often tens of thousands of
        dimensions) Euclidean vector space.
      \item Our mental model suggest that the data actually live on (or
        near) a much smaller dimensional manifold.
      \item \textcolor{accent2color}{Curse of Dimensionality:} when
        the dimensionality increases, the volume of the space
        increases so fast that the available data become sparse.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item \textcolor{accent1color}{Ambient Dimension:} the number
        of features (genes, proteins, etc.) measured.
      \item \textcolor{accent1color}{PC Dimension} The dimension of a
        principal component (PC) space that explains variability in
        the data.
      \item \textcolor{accent1color}{Local Dimension} The dimension
        $N$ of the underlying manifold near one point of the space. If
        only one component, constant.
      \item \textcolor{accent1color}{Embedding Dimenson} The smallest
        Euclidean space that can hold the topological manifold. By the
        Whitney Embedding Theorem, at most $2N$.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{PCDimension}
\begin{frame}
  \frametitle{PCDimension R Package}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=80mm]{Figures/PCDimension}
      \end{center}
      \begin{itemize}
      \item Implements and automates a graphical Bayesian method
        (\textcolor{accent2color}{Auer-Gervini, 2008}) to
        compute significant PC dimension.
      \item Applied to an RPPA data set focused on apoptosis in 500+
        AML samples.
      \item \textcolor{accent2color}{Biological Point: Pathways are
        not on-off switches.}
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=60mm]{Figures/apoptosis}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Priors}
      \begin{center}
        \includegraphics[width=150mm]{Figures/ag-priors}
      \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Posteriors}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=70mm]{Figures/agplot}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=60mm]{Figures/apoptosis-cleaned-04A-loadings}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Local Dimension}
\begin{frame}
  \frametitle{}
  \begin{columns}
    \begin{column}{0.5\textwidth} 
      \begin{itemize}
      \item \textcolor{accent2color}{Ellis and McDermott, Measuring
        the Local Dimension of Point Clouds, Comp Stat \& Data Analy,
        1994; 17(3):317-326.}
      \item Use the ``Takens estimate'' (1981, 1985) to compute local
        dimensions of point-cloud data.
      \item Implemented in the Mender R Package, which also includes
        tools to perform topological data analysis in order to find
        loops, voids, and higher dimensional structures.
      \item Applied to CyTOF data fom one sample with 330,000 cells
        and 43 measured proteins.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=60mm]{Figures/ldpc-PCA}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{CyTOF, Auer-Gervini}
  \begin{center}
    \includegraphics[width=110mm]{Figures/ldpc-AG}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{CyTOF, Proliferation (Ki-67)}
  \begin{center}
    \includegraphics[width=140mm]{Figures/ldpc-Ki67}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{CyTOF, Known Cell Types}
  \begin{center}
    \includegraphics[width=130mm]{Figures/ldpc-cellTypes}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{CyTOF, Takens Estimates}
  \begin{center}
    \includegraphics[width=140mm]{Figures/ldpc-est}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{CyTOF, Local Dimension by Cell Type}
  \begin{center}
    \includegraphics[width=150mm]{Figures/ldpc-beans}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Conclusions}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item (\textcolor{accent2color}{Polychrome}) It's worth spending
        time picking out colors that are more likely to be
        distinguishable by average humans.
      \item (\textcolor{accent2color}{Mercator}) Multiple views of the
        same data help understand its structure.
        \begin{itemize}
        \item Different clustering algorithms can yield very different
          results.
        \item Cramming higher-dimensional data into 2D can destroy
          topological structure and induce artifacts.
        \end{itemize}
      \item (\textcolor{accent2color}{SVAlignR}) We can combine short-read
        and long-read sequencing to disentangle structural variants
        and tumor evolution.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item (\textcolor{accent2color}{Umpire}) We can simulate more
        realistic, and complex, data sets using a tool inspired by
        biological principles.
      \item (\textcolor{accent2color}{SillyPutty}) The best and
        fastest clustering algorithm is hierarchical clustering
        followed by SillyPutty.
      \item (\textcolor{accent2color}{PCDimension}) Estimating PC
        dimension, we can decompose pathways into 1D components.
      \item (\textcolor{accent2color}{Mender}) Biological data sets
        composed of pieces of different dimensions, depending on cell
        type and proliferation.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Lab Members}
  { \tiny

  \begin{tabular}{c c c c c c}
    \parbox[t]{20mm}{\includegraphics[height=15mm]{../../people/images/polina-bombina}\\
    Polina Bombina\\Postdoc, 2023-now} &

    \parbox[t]{20mm}{\includegraphics[height=15mm]{../../people/images/jakereed}\\
    Jake Reed\\Postdoc, 2022-now} &

    \parbox[t]{20mm}{\includegraphics[height=15mm]{../../people/images/zach-abrams}\\
    Zach Abrams\\Postdoc, 2016-21} &

    \parbox[t]{20mm}{\includegraphics[height=15mm]{../../people/images/amir-asiaee}\\
      Amir Asiaee\\Postdoc, 2017-21} &

    \parbox[t]{20mm}{\includegraphics[height=15mm]{../../people/images/mcgee}\\
      RB McGee\\Postdoc, 2015-17} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/min-wang}\\
     Min Wang\\Postdoc, 2015-17}
   \cr&&&&&\cr
   \vspace{2mm}

   % row two
   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/pan-tong}\\
     Pan Tong\\PhD, 2009-2013} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/mark-zucker}\\
     Mark Zucker\\PhD, 2013-2018} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/gayathri-warrier}\\
     Gayathri Warrier\\MS, 2017} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/cat-coombes}\\
     Caitlin Coombes\\MS, 2020} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/joglekar}\\
     Anoushka Jogekar\\Intern, 2017} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/suli-li}\\
     Suli Li\\Intern, 2018}\cr
   \vspace{2mm}
   % row three
   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/tommy_bai}\\
     Tommy Bai\\Intern, 2018} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/gershkowitz}\\
     Greg Gershkowitz\\Intern, 2019} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/sam-nakayiza}\\
     Samantha Nakayiza\\Intern, 2019} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/avatar}\\
     Dwayne Tally\\Intern, 2018-2022} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/phillip-nicol}\\
     Phillip Nicol\\Intern, 2019-2020} &

   \parbox[t]{20mm}{ \includegraphics[height=15mm]{../../people/images/gus-gerlach}\\
     Gus Gerlach\\Intern, 2022-now} \\
\end{tabular}
  }
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Coombes Lab R Packages}
  \begin{columns}
    \begin{column}{0.35\textwidth}
      \begin{center}
        \includegraphics[width=30mm]{Figures/cranList}
      \end{center}
    \end{column}
    \begin{column}{0.65\textwidth}
      \begin{itemize}
      \item Can get a list of all released packages by searching
        ``CRAN Coombes''
      \item
        \url{https://cran.r-project.org/web/checks/check_results_krc_at_silicovore.com.html}
      \item To include projects in development, search for ``R-Forge
        krcoombes''
      \item \url{https://r-forge.r-project.org/users/krcoombes/}
      \item Can find published papers by a PubMed search for ``coombes
        kr[au]''
      \item \url{https://pubmed.ncbi.nlm.nih.gov/?term=coombes+kr\%5Bau\%5D\&sort=pubdate}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain,noframenumbering]
 \frametitle{Contents}
  \begin{multicols}{2}
    \tableofcontents
  \end{multicols}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Color Deficit}
  \begin{center}
    \includegraphics[height=72mm]{Figures/colorDeficit}
  \end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Apoptosis Protein Clusters}
  \begin{columns}
    \begin{column}{0.3333\textwidth}
      \begin{center}
        \includegraphics[width=48mm]{Figures/apoptosis-cleaned-04A-loadings}
      \end{center}
    \end{column}
    \begin{column}{0.3333\textwidth}
      \begin{center}
        \includegraphics[width=48mm]{Figures/apoptosis-cleaned-04B-loadings}
      \end{center}
    \end{column}
    \begin{column}{0.3333\textwidth}
      \begin{center}
        \includegraphics[width=48mm]{Figures/apoptosis-cleaned-04C-loadings}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}


\end{document}

%%%%%%%%%%%%  TEMPLATES   %%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{}
  \begin{columns}
    \begin{column}{0.5\textwidth}
    \end{column}
    \begin{column}{0.5\textwidth}
    \end{column}
  \end{columns}
\end{frame}


