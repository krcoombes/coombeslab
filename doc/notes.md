---
author: Kevin R. Coombes
title: Using the Gitlab Web Interface
date: 2020-04-12
---

# General Advice

## Branch Structure
All of our projects are set up at the beginning with multiple
branches. In genral, we have three branches:

+ **master**
This is the core branch, and it is usually _protected_. That means
that only the owner of the project can change it. We rely on this to
ensure that there is always a stable branch to fall back on, just in case we
do something stupid in a different branch.
+ **develop**
This is the primary branch to use for any changes that involve
code, scripts, etc. Most code will be written using R markdown, and
stored in the `code` subfolder.
+ **manuscript**
This is the primary branch for manuscripts that result from the
project. Manuscripts are stored in the `doc` subfolder.
Ideally, manuscripts are written using LaTeX, with references
maintained in EndNote but exported to BibTeX format. We will grumpily
accede to having manuscripts written in Word, even though it is several
orders of magnitude less git-friendly. Besides, `pandoc` can convert
back-and-forth between Word and LaTeX (and markdown or html for that
matter). So, you're welcome to use Word in the privacy of your own
home. But you really shouldn't inflict it on others.

## The Web Interface

Before using the "plus sign" icon to open the upload dialog (from the
`Files` page in the web interface), you should first make sure that
the correct non-master branch is selected. (As noted above, `master`
is usually protected so that we can mess up the other branches as much
as we want or need to.) Then drag the file to be uploaded as usual.
But *before* pushing the upload button, change the default "Upload new
file" message to something that explains exactly what you are
uploading and why. (You can also check that the "target branch" is
listed correctly at this point.)

* Edits to manuscripts, or new manuscript related files, should go to
  the `manuscript` branch, in an appropriate folder.
* Edits to scripts, or new scripts or code-related files, should go to
  the `develop` branch, in an appropriate folder.

*Note:* If you upload a file with  the same name as an existing file,
then git "does the right thing". Namely, it replaces the old one with
the new one, and remembers the differences. That means that you still
can't break things even if you try, because the old version can always
be recovered. 

## Possible Data Sources

"For people looking for data, I'd like to share this website
https://datamed.org/ which indexes bioCADDIE (biomedical and
healthCAre Data Discovery Index Ecosystem). It's a (cumbersome) pile
of data sets, with the caveat that many need to be requested. Hope
someone finds it useful."  -- from Cat

"I found this article on clustering biomedical data that provides an
excellent outline of the predominant techniques. It could be good for
getting a student acclimated, but it also could be a good reference to
cite in a paper for an easy definition. 
https://ieeexplore-ieee-org.proxy.lib.ohio-state.edu/document/5594620"
-- Cat

"Quantifying a known fact: 
The drop in citations from non-theory papers to your paper for each
extra equation you include = 28%.   
https://www.pnas.org/content/109/29/11735  " --Amir
