---
author: Kevin R. Coombes
date: 30 October 2018
title: Getting Started
---

# Overview
This document describes software tools (for Windows machines) that you
should install, and start using, to collaborate on Coombes lab
projects. Similar (and often identical) software is available for
UNIX/LINUX machines. Since the latest MacOS is a flavor of UNIX, those
tools shouold also be availabel for Macs. However, the following
instructions are designed specifically for Windows.

You can mostly work your way through this document in order.

## Secure Shell (SSH) Client
The SSH client that I recommend for a Windows machine is
PuTTY. However, this comes with a warning that it is not really a
modern GUI-based interface, and is not particularly well integrated
into the Windows ecosystem. Our primary use for this set of programs
is to connect securely to remote code repositories, including R-Forge
and GitLab. It can also be used to assist with an X-Windows connection
to the Ohio Supercomputer Center (OSC) (or anywhere else that X is
used, of coursee).

A critical part of making the connections secure is to create a
public/private key pair, which you do as follows:

1. Download PuTTY and its relatives (including PuTTYgen and Pageant)
   from the [PuTTY web
   site](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html). 
   You want to get a zip file containing all of the utilities.
2. Unzip the downloaded file and copy everything in it to some folder
   on your computer. (I just put it into `C:/PuTTY`.)
3. (**One time only.**) Double click the icon for the executable file
   `puttygen` to start the public-private key generator. Push the
   `generate` button and follow the instructions (moving the mouse
   randomly) to create a key. (You can use the default selection of an
   `SSH-2 RSA` key type.)
4. **Important:** Make sure you save both the public key and the
   private key into files on your computer, probably in the
   `Documents` folder. Also make sure they have similar names. Mine
   are saved as `krcPublic.txt` and `krcPrivate.ppk`. When you save
   the private key, you need to provide a pass phrase (which you will
   have to type once for each time that you start your computer and
   want to use the key pair. This phrase doen't need to satisfy any of
   the conventions of short passwords, since it can be as long as you
   like. For example, you can just put together four (lowercase) English
   words, like `rainbow unicorns sniff concrete`.
5. **Important:** The private key should be kept, well, private. But the
   public key can be safely posted on a web site. Here's mine:

<pre>
Comment: "rsa-key-20140131"
AAAAB3NzaC1yc2EAAAABJQAAAQEAqgIUMvodNuk6z08HIsJwg5w9nQMh19KAtnWy
MPekCjC6tRs8cpDxURzWi4ZJH4uXm7nTsGEAYlvl28GJM70bsWbRRkoDtZvDRy+2
6VVq4Z0Rv2codFX7cqIfklozFCuSisTM2PSkYBKBk7gWLjKnT/Ia2is28D6CX8xT
YCgukUYdKAq1UXysIiz8Wg4Ab1cdOFqyfFQ2isKfK8HW1dU9nBKX37UOvwtMhkHc
q4OVaEQ0NAFoKV0fFaKifJG61erhR8h7+hCiVPg5QRn+IDSiIBvA1It571xm0rfl
7oBlyQKkkznbwcjRYjRKsPYtNjnj0quw2f6KcFX847MiGuuWXQ==
---- END SSH2 PUBLIC KEY ----
</pre>

You can get more information from the online
[PuTTY user manual](https://the.earth.li/~sgtatham/putty/0.70/htmldoc/)

## Code Repositories
At present, we have code (and text) repositories scattered across the web,
in at least three locations:

1. The main public [GitLab server](https://gitlab.com/)
2. A private [OSU/BMI GitLab Server](https://code.bmi.osumc.edu/)
3. The Subversion-based repository for developing R packages at
   [R-Forge](https://r-forge.r-project.org/)

If you don't already have accounts at those places (most people
don't, of course), then you should follow the links and create
accounts. The public GitLab and R-Forge sites are easy, since the
proess is automated (but requires an email verification). 

The OSU/BMI server is more complicated, since it requres manual
intervention to activate the account. **Important:** When you sign
into the OSU/BMI server (the first time it will start to create an
account), do **not** use the big, obvious, in-your-face green sign-in
button. Instead, use the hardly noticeable "Ohio State Name.#" sign-in
button just below the green button. You should then follow the
instructions, which at the time of this writing are to send an
email to ShunChao ("Chao") Wang asking for activation. Please copy me
on that email so I can assure him that you really need the account.

## 
You will need to tell GitlLab and R-Forge about your public key. In
GitLab, you do this by

1. Click on the icon in the upper right-hand corner to go to "My
   Profile". 
2. Click on the "pencil" icon to edit the profile.
3. Click on the "SSH" link in the left hand menu.
4. Paste your **public** key into the appropriate box.

**Note:** There are two differently "decorated" versions of the public
key. GitLab probably wants one that looks like this:

<pre>
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAqgIUMvodNuk6z08HIsJwg5w9nQMh19KAtnWy
MPekCjC6tRs8cpDxURzWi4ZJH4uXm7nTsGEAYlvl28GJM70bsWbRRkoDtZvDRy+2
6VVq4Z0Rv2codFX7cqIfklozFCuSisTM2PSkYBKBk7gWLjKnT/Ia2is28D6CX8xT
YCgukUYdKAq1UXysIiz8Wg4Ab1cdOFqyfFQ2isKfK8HW1dU9nBKX37UOvwtMhkHc
q4OVaEQ0NAFoKV0fFaKifJG61erhR8h7+hCiVPg5QRn+IDSiIBvA1It571xm0rfl
7oBlyQKkkznbwcjRYjRKsPYtNjnj0quw2f6KcFX847MiGuuWXQ== rsa-key-20140131
</pre>

If you only have one that looks like the version earlier, edit it so it
looks like this one. (The most important part is the gobbledy-gook
that starts "AAAA" and ends "XQ==" in my key. It's the part around
that that you need to reformat.)

## Rstudio and Subversion
The RStudio IDE includes built-in tools for interacting with code
repositories using either git (at GitHub or GitLab) or subversion (at
R-Forge, for example). Getting things setup to work properly ona
Windows machine can be compklicted, however. Here's what worked for
me:

1. You must reinstall TortoiseSVN, being careful to include the
   command line tools so you have a local cop of svn.exe
2. Open PuttyGen and load your existing RSA private key. (Enter your
   passphrase when requested.) Then use the "Conversions" menu to
   export ssh version (not the OpenSHH).
3. Move or copy your keys to `C:/Users/[UNAME]/.ssh`. Rename the
   private key to `id_rsa`, and rename the public key to `id_rsa.pub`.
4. Configure RStudio (via Tools -> Global Options -> GIT/SVN) to use
   the `ssh.exe` and the exported ssh key.
5. Copy the repositoruy URL from R-Forge.
6. Open RStudio, go to "File -> New Project"
7. Select "Version Control", then "Subversion"
8. Paste the copied URL into the top box. Change the protocol preface
   from "svn+ssh:" to just "svn:". Put your R-Forge user name into the
   second box.
9. Select a directory to which you want to clone the repository. Then
   click "Create Ptroject".

If you only want to use git and not svn, you still have to perfor the
first four steps so that git can find and use your existing key-pair.

## Install TortoiseSVN

## Install TortoiseGit

## Install the Git Client

### Version Control
Both Git and Subversion (SVN) are kinds of [version control
systems](https://en.wikipedia.org/wiki/Version_control). Version
control has been used by computer scientists and programmers for
decades. 


# LaTeX, Pandoc, etc

## Install Pandoc
Go to the [pandoc home page](https://pandoc.org/).

Follow the "Installing" and "Download page" links to get to their
[Github-hosted download
page](https://github.com/jgm/pandoc/releases/tag/2.5)

Follow their instructions to install.

## Install GNU Make for Windows
GNU Make for Windows (get it
[here](http://www.equation.com/servlet/equation.cmd?fa=make)) Copy the
executable file into some folder that is on the Windows executable
search path.


Start at the main
[Make for Windows](http://gnuwin32.sourceforge.net/packages/make.htm)
web page. Download the **Setup** file containing "Complete package,
except sources". This step downloads a file called (something like)
`make-3.18.exe`. Run that file and follow the instruction to install
the program.

## Install MikTeX

MikTeX is the main Windows-based implementation of the TeX and LaTeX
word processing systems. As usual, you can start at the [MikTeX home
page](https://miktex.org/). Then follow the link for thie instruction
to install MikTeX on your kind of computer. Follow their instructions
to install a "basic MikTeX" system, probably for "all users".


