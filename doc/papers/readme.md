---
author: Kevin R.Combes
date: 2020-04-12
title: Coombes Lab / Papers
---

# Papers
This folder contains information about publications from the
lab. Papers listed here are those in which lab members played a major
role in developing new anaysis methods; more straightforward
applications of existing methods to omics data are omitted.

Names in **boldface** indicate lab members or other trainees.

## BioRxiv Preprints
1. **Gershkowitz GR**, **Abrams ZB**, **Coombes CE**, **Coombes KR**. [Malachite: A Gene Enrichment Meta-Analysis (GEM) Tool for ToppGene.](malachite-bioRxiv.pdf) Preprint.

1. **Abrams ZB**, **Coombes CE**, **Li S**, **Coombes KR**. [Mercator: An R Package for Visualization of
Distance Matrices.](mercator-bioRxiv.pdf) Preprint.
1. **Nicol PB**, **Coombes KR**, Deaver C, Chkrebtii OA, Paul S,
   Toland AE, **Asiaee A**. [Oncogenetic Network Estimation with Disjunctive Bayesian
Networks: Learning from Unstratified Samples while Preserving
Mutual Exclusivity Relations](DBN.pdf). Preprint.
1. **Coombes KR**, Brock G, **Abrams ZB**, Abruzzo LV. [Polychrome: Creating
and Assessing Qualitative Palettes With Many Colors.](Polychrome-bioRxiv.pdf) J
Statist Softare. 2019;90:Code Snippet 1.
1. **Asiaee A**, **Abrams ZB**, **Nakayiza S**, Sampath D, **Coombes
KR**. [Identification and comparison of genes differentially regulated by transcription factors and miRNAs.](TF-miR-bioRxiv.pdf) Preprint;
published as: Explaining Gene Expression Using Twenty-One
MicroRNAs. **J Comput Biol.** 2019 Dec 2. doi:
10.1089/cmb.2019.0321. [Epub ahead of
print] [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/31794247).
1. **Abrams ZB**, Johnson TS, Huang K, Payne PRO, **Coombes K**. [A protocol to evaluate
RNA sequencing normalization methods.](normalization-BMCBioinfo.pdf)
**BMC Bioinformatics**. 2019 Dec 20;20(Suppl
24):679. doi. 10.1186/s12859-019-3247-x. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/31861985); PMCID. PMC6923842.
4. **Abrams ZB**, Zhang L, Abruzzo LV, Heerema NA, Li S, Dillon T, Rodriguez R,
**Coombes KR**, Payne PRO. [CytoGPS. a web-enabled karyotype analysis tool for
cytogenetics.](CytoGPS-bioRxiv.pdf) **Bioinformatics**. 2019 Dec 15;35(24):5365-5366. doi:
10.1093/bioinformatics/btz520. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/31263896); PMCID:
PMC6954647.
8. **Wang M**, Kornblau SM, **Coombes KR**. [Decomposing the Apoptosis Pathway Into
Biologically Interpretable Principal Components.](PCDimension-bioRxiv.pdf) **Cancer
Inform**. 2018 May 9;17:1176935118771082. doi. 10.1177/1176935118771082. eCollection 2018. PubMed
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/29881252); PMCID. PMC5987987.


## Recent (OSU)

1. **Nicol PB**, Barabási DL, **Coombes KR**, **Asiaee A**. SITH: An R
  package for visualizing and analyzing a spatial model of intratumor
  heterogeneity. Comput Syst Oncol. 2022 Jun;2(2):e1033. 
  [DOI](https://doi.org/10.1002/cso2.1033). 
  [PMID](https://pubmed.ncbi.nlm.nih.gov/35966389/)

1. Symer DE, Akagi K, Geiger HM, Song Y, Li G, Emde AK, Xiao W, Jiang
   B, Corvelo A, Toussaint NC, Li J, Agrawal A, Ozer E, El-Naggar AK,
   Du Z, Shewale JB, Stache-Crain B, **Zucker M**, Robine N, **Coombes
   KR**, Gillison ML. _Diverse tumorigenic consequences of human
   papillomavirus integration in primary oropharyngeal cancers._ Genome
   Res. 2022 Jan;32(1):55-70. [DOI](https://doi.org/10.1101/gr.275911.121).
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/34903527)

1. **Abrams ZB**, **Tally DG**, Abruzzo LV, **Coombes KR**. 
   _RCytoGPS: An R Package for Reading and Visualizing Cytogenetics Data._
   Bioinformatics. 2021 Oct 2:btab683.
   [DOI](https://doi.org/10.1093/bioinformatics/btab683).
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/34601554).
1. **Coombes CE**, Liu X, **Abrams ZB**, **Coombes KR**, Brock G.
   _Simulation-derived best practices for clustering clinical data._
   J Biomed Inform. 2021 Jun;118:103788.
   [DOI](https://doi.org/10.1016/j.jbi.2021.103788). 
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/33862229)
1. **Coombes CE**, **Coombes KR**, Fareed N.
   _A novel model to label delirium in an intensive care unit from clinician actions._
   BMC Med Inform Decis Mak. 2021 Mar 9;21(1):97.  [DOI](https://doi.org/10.1186/s12911-021-01461-6).
  [PMID](https://www.ncbi.nlm.nih.gov/pubmed/33750375)
1. Giacopelli B, **Wang M**, Cleary A, Wu YZ, Schultz R, Schmutz M, Blachly JS, Eisfeld AK,
   Mundy-Bosse B, Vosberg S, Greif PA, Claus R, Bullinger L, Garzon R, **Coombes KR**,
   Bloomfield CD, Druker BJ, Tyner JW, Byrd JC, Oakes CC.
   _DNA methylation epitypes highlight underlying developmental and disease pathways in
   acute myeloid leukemia._  Genome Res. 2021 May;31(5):747-761.
   [DOI](https://doi.org/10.1101/gr.269233.120).
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/33707228).
1. **Abrams ZB**, Tally DG, Zhang L, **Coombes CE**, Payne PRO, Abruzzo LV, **Coombes KR**.
   _Pattern recognition in lymphoid malignancies using CytoGPS and Mercator._
   BMC Bioinformatics. 2021 Mar 1;22(1):100. [DOI](https://doi.org/10.1186/s12859-021-03992-1.)
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/33648439)
1. **Abrams ZB**, **Coombes CE**, Li S, **Coombes KR**.
   _Mercator: A Pipeline For Multi-Method, Unsupervised Visualization And Distance Generation._
   Bioinformatics. 2021 Jan 30;37(17):2780-1. [DOI](https://doi.org/10.1093/bioinformatics/btab037).
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/33515233)
1. Saji M, Kim CS, Wang C, Zhang X, Khanal T, **Coombes K**, La Perle K, Cheng SY, Tsichlis PN, Ringel MD.
   _Akt isoform-specific effects on thyroid cancer development and progression in a murine thyroid cancer model._
   Sci Rep. 2020 Oct 27;10(1):18316. [DOI](https://doi.org/10.1038/s41598-020-75529-0).
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/33110146).
1. **Abrams ZB**, **Li S**, Zhang L, **Coombes CE**, Payne PRO, Heerema NA, Abruzzo LV, **Coombes KR**.
   _CytoGPS: A large-scale karyotype analysis of CML data._
   Cancer Genet. 2020
   Oct;248-249:34-38. [DOI](https://doi.org/10.1016/j.cancergen.2020.09.005).
   [PMID](https://www.ncbi.nlm.nih.gov/pubmed/33059160).
1. Hu EY, Blachly JS, Saygin C, Ozer HG, Workman SE, Lozanski A, Doong TJ, Chiang CL,
	Bhat S, Rogers KA, Woyach JA, **Coombes KR**, Jones D, Muthusamy N, Lozanski G, Byrd JC.
	_LC-FACSeq is a method for detecting rare clones in leukemia._
	JCI Insight. 2020 Jun 18;5(12):e134973. [DOI](https://doi.org/10.1172/jci.insight.134973).
	[PMID](https://www.ncbi.nlm.nih.gov/pubmed/32554930)
1. **Coombes CE**, Abrams ZB, Li S, Abruzzo LV, **Coombes KR**.
	_Unsupervised machine learning and prognostic factors of survival in chronic lymphocytic leukemia._
	J Am Med Inform Assoc. 2020 Jul 1;27(7):1019-1027. [DOI](https://doi.org/10.1093/jamia/ocaa060).
	[PMID](https://www.ncbi.nlm.nih.gov/pubmed/32483590)

## Older (OSU)

12. **Abrams ZB**, Johnson TS, Huang K, Payne PRO, **Coombes K**.
	_A protocol to evaluate RNA sequencing normalization methods._
	BMC Bioinformatics. 2019 Dec 20;20(Suppl 24):679. [DOI](https://doi.org/10.1186/s12859-019-3247-x).
	[PMID](https://www.ncbi.nlm.nih.gov/pubmed/31861985).

1. **Asiaee A**, **Abrams ZB**, **Nakayiza S**, Sampath D, **Coombes KR**.
	_Explaining Gene Expression Using Twenty-One MicroRNAs._
	J Comput Biol. 2020 Jul;27(7):1157-1170. [DOI](https://doi.org/10.1089/cmb.2019.0321).
	[PMID](https://www.ncbi.nlm.nih.gov/pubmed/31794247).
1. Herling CD, **Coombes KR**, Benner A, Bloehdorn J, Barron LL, **Abrams ZB**, Majewski T, Bondaruk JE, Bahlo J,
	Fischer K, Hallek M, Stilgenbauer S, Czerniak BA, Oakes CC, Ferrajoli A, Keating MJ, Abruzzo LV.
	_Time-to-progression after front-line fludarabine, cyclophosphamide, and rituximab chemoimmunotherapy for
	chronic lymphocytic leukaemia: a retrospective, multicohort study._
	Lancet Oncol. 2019 Nov;20(11):1576-1586. [DOI](https://doi.org/10.1016/S1470-2045(19)30503-0).
	[PMID](https://www.ncbi.nlm.nih.gov/pubmed/31582354).
1. Giacopelli B, Zhao Q, Ruppert AS, Agyeman A, Weigel C, Wu YZ, Gerber MM, Rabe KG, Larson MC, Lu J, Blachly JS,
	Rogers KA, Wierda WG, Brown JR, Rai KR, Keating M, Rassenti LZ, Kipps TJ, Zenz T, Shanafelt TD, Kay NE, Abruzzo
	LV, **Coombes KR**, Woyach JA, Byrd JC, Oakes CC.
	_Developmental subtypes assessed by DNA methylation-iPLEX forecast the natural history of chronic lymphocytic
	leukemia._
	Blood. 2019 Aug 22;134(8):688-698. [DOI](https://doi.org/10.1182/blood.2019000490). 
	[PMID](https://www.ncbi.nlm.nih.gov/pubmed/31292113)
1. **Abrams ZB**, Zhang L, Abruzzo LV, Heerema NA, **Li S**, Dillon T, Rodriguez R, **Coombes KR**, Payne PRO.
	_CytoGPS: a web-enabled karyotype analysis tool for cytogenetics._
	Bioinformatics. 2019 Dec 15;35(24):5365-5366. [DOI](https://doi.org/10.1093/bioinformatics/btz520).
	[PMID](https://www.ncbi.nlm.nih.gov/pubmed/31263896)
1. **Zucker MR**, Abruzzo LV, Herling CD, Barron LL, Keating MJ, **Abrams ZB**, Heerema
N, **Coombes KR**. Inferring clonal heterogeneity in cancer using SNP arrays and
whole genome sequencing. Bioinformatics. 2019 Sep 1;35(17):3216. doi:
10.1093/bioinformatics/btz243. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/31161200); PMCID:
PMC6736449.

1. **Abrams ZB**, **Zucker M**, **Wang M**, Asiaee Taheri A, Abruzzo LV, **Coombes KR**. Thirty
biologically interpretable clusters of transcription factors distinguish cancer
type. BMC Genomics. 2018 Oct 11;19(1):738. doi. 10.1186/s12864-018-5093-z. PubMed
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/30305013); PMCID. PMC6180590.
1. Abruzzo LV, Herling CD, Calin GA, Oakes C, Barron LL, Banks HE, Katju V,
Keating MJ, **Coombes KR**. Trisomy 12 chronic lymphocytic leukemia expresses a
unique set of activated and targetable pathways. Haematologica. 2018
Dec;103(12):2069-2078. doi. 10.3324/haematol.2018.190132. Epub 2018 Jul 5. PubMed
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/29976738); PMCID. PMC6269288.
1. **Wang M**, Kornblau SM, **Coombes KR**. Decomposing the Apoptosis Pathway Into
Biologically Interpretable Principal Components. Cancer Inform. 2018 May
9;17:1176935118771082. doi. 10.1177/1176935118771082. eCollection 2018. PubMed
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/29881252); PMCID. PMC5987987.
1. Siddiqui JK, Baskin E, Liu M, Cantemir-Stone CZ, Zhang B, Bonneville R,
McElroy JP, **Coombes KR**, Mathé EA. IntLIM. integration using linear models of
metabolomics and gene expression data. BMC Bioinformatics. 2018 Mar 5;19(1):81.
doi. 10.1186/s12859-018-2085-6. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/29506475); PMCID:
PMC5838881.
1. **Wang M**, **Abrams ZB**, Kornblau SM, **Coombes KR**. Thresher. determining the number 
of clusters while removing outliers. BMC Bioinformatics. 2018 Jan 8;19(1):9. doi:
10.1186/s12859-017-1998-9. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/29310570); PMCID:
PMC5759208.

## Older (M. D. Anderson)

23. **Tong P**, Diao L, Shen L, Li L, Heymach JV, Girard L, Minna JD, **Coombes KR**,
Byers LA, **Wang J**. Selecting Reliable mRNA Expression Measurements Across
Platforms Improves Downstream Analysis. Cancer Inform. 2016 May 10;15:81-9. doi. 
10.4137/CIN.S38590. eCollection 2016. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/27199546); PMCID. PMC4863871.

1. **Tong P**, **Coombes KR**, Johnson FM, Byers LA, Diao L, Liu DD, Lee JJ, Heymach JV,
**Wang J**. drexplorer. A tool to explore dose-response relationships and drug-drug
interactions. Bioinformatics. 2015 May 15;31(10):1692-4. doi:
10.1093/bioinformatics/btv028. Epub 2015 Jan 18. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/25600946);
PMCID. PMC4426846.
1. Kornblau SM, Qutub A, Yao H, York H, Qiu YH, Graber D, Ravandi F, Cortes J,
Andreeff M, Zhang N, **Coombes KR**. Proteomic profiling identifies distinct protein 
patterns in acute myelogenous leukemia CD34+CD38- stem-like cells. PLoS One. 2013
Oct 24;8(10):e78453. doi. 10.1371/journal.pone.0078453. eCollection 2013. 
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/24223100); PMCID. PMC3816767.
1. **Manyam G**, Ivan C, Calin GA, **Coombes KR**. targetHub. a programmable interface
for miRNA-gene interactions. Bioinformatics. 2013 Oct 15;29(20):2657-8. doi:
10.1093/bioinformatics/btt439. Epub 2013 Sep 6. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/24013925);
PMCID. PMC3789542.
1. Wang W, Baggerly KA, Knudsen S, Askaa J, Mazin W, **Coombes KR**. Independent
validation of a model using cell line chemosensitivity to predict response to
therapy. J Natl Cancer Inst. 2013 Sep 4;105(17):1284-91. doi:
10.1093/jnci/djt202. Epub 2013 Aug 20. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/23964133); PMCID. PMC3955959.
1. **Zhang J**, **Coombes KR**. Sources of variation in false discovery rate estimation 
include sample size, correlation, and inherent differences between groups. BMC
Bioinformatics. 2012;13 Suppl 13:S1. doi. 10.1186/1471-2105-13-S13-S1. Epub 2012
Aug 24. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/23320794); PMCID. PMC3426804.
1. **Tong P**, Chen Y, Su X, **Coombes KR**. SIBER. systematic identification of
bimodally expressed genes using RNAseq data. Bioinformatics. 2013 Mar
1;29(5):605-13. doi. 10.1093/bioinformatics/bts713. Epub 2013
Jan 9. [PMID](https://www.ncbi.nlm.nih.gov/pubmed/23303507); PMCID. PMC3582265.
1. Schweighofer CD, **Coombes KR**, Majewski T, Barron LL, Lerner S, Sargent RL,
O'Brien S, Ferrajoli A, Wierda WG, Czerniak BA, Medeiros LJ, Keating MJ, Abruzzo LV. Genomic variation by whole-genome SNP mapping arrays predicts time-to-event
outcome in patients with chronic lymphocytic leukemia. a comparison of CLL and
HapMap genotypes. J Mol Diagn. 2013 Mar;15(2):196-209. doi:
10.1016/j.jmoldx.2012.09.006. Epub 2012 Dec 27. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/23273604);
PMCID. PMC3586684.
1. **Tong P**, **Coombes KR**. integIRTy. a method to identify genes altered in cancer
by accounting for multiple mechanisms of regulation using item response theory.
Bioinformatics. 2012 Nov 15;28(22):2861-9. doi. 10.1093/bioinformatics/bts561.
Epub 2012 Sep 26. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/23014630); PMCID. PMC3496341.
1. **Manyam G**, Payton MA, Roth JA, Abruzzo LV, **Coombes KR**. Relax with
CouchDB--into the non-relational DBMS era of bioinformatics. Genomics. 2012
Jul;100(1):1-7. doi. 10.1016/j.ygeno.2012.05.006. Epub 2012
May 17. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/22609849); PMCID. PMC3383915.
1. Schweighofer CD, **Coombes KR**, Barron LL, Diao L, Newman RJ, Ferrajoli A,
O'Brien S, Wierda WG, Luthra R, Medeiros LJ, Keating MJ, Abruzzo LV. A two-gene
signature, SKI and SLAMF1, predicts time-to-treatment in previously untreated
patients with chronic lymphocytic leukemia. PLoS One. 2011;6(12):e28277. doi:
10.1371/journal.pone.0028277. Epub 2011 Dec 14. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/22194822);
PMCID. PMC3237436.
1. Kornblau SM, **Coombes KR**. Use of reverse phase protein microarrays to study
protein expression in leukemia. technical and methodological lessons learned.
Methods Mol Biol. 2011;785:141-55. doi. 10.1007/978-1-61779-286-1_10. Review.
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/21901598); PMCID. PMC4554538.

## Ancient History (at least ten years old)

35. Baggerly KA, **Coombes KR**. What information should be required to support
clinical "omics" publications? Clin Chem. 2011 May;57(5):688-90. doi:
10.1373/clinchem.2010.158618. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/21364027).

1. Duzkale H, Schweighofer CD, **Coombes KR**, Barron LL, Ferrajoli A, O'Brien S,
Wierda WG, Pfeifer J, Majewski T, Czerniak BA, Jorgensen JL, Medeiros LJ,
Freireich EJ, Keating MJ, Abruzzo LV. LDOC1 mRNA is differentially expressed in
chronic lymphocytic leukemia and predicts overall survival in untreated patients.
Blood. 2011 Apr 14;117(15):4076-84. doi. 10.1182/blood-2010-09-304881. Epub 2011 
Feb 10. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/21310924); PMCID. PMC3087532.
1. **Wang J**, Byers LA, Yordy JS, Liu W, Shen L, Baggerly KA, Giri U, Myers JN, Ang
KK, Story MD, Girard L, Minna JD, Heymach JV, **Coombes KR**. Blasted cell line
names. Cancer Inform. 2010 Oct 14;9:251-5. doi. 10.4137/CIN.S5613. [PMID[(https://www.ncbi.nlm.nih.gov/pubmed/21082038); PMCID. PMC2978931.
1. Kornblau SM, McCue D, Singh N, Chen W, Estrov Z, **Coombes KR**. Recurrent
expression signatures of cytokines and chemokines are present and are
independently prognostic in acute myelogenous leukemia and myelodysplasia. Blood.
2010 Nov 18;116(20):4251-61. doi. 10.1182/blood-2010-01-262071. Epub 2010 Aug 2. 
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/20679526); PMCID. PMC4081283.
1. Morris JS, Baggerly KA, Gutstein HB, **Coombes KR**. Statistical contributions to
proteomic research. Methods Mol Biol. 2010;641:143-66. doi:
10.1007/978-1-60761-711-2_9. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/20407946); PMCID:
PMC3889133.
1. Kornblau SM, Singh N, Qiu Y, Chen W, Zhang N, **Coombes KR**. Highly
phosphorylated FOXO3A is an adverse prognostic factor in acute myeloid leukemia. 
Clin Cancer Res. 2010 Mar 15;16(6):1865-74. doi. 10.1158/1078-0432.CCR-09-2551.
Epub 2010 Mar 9. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/20215543); PMCID. PMC3385949.
1. **Wang J**, Wen S, Symmans WF, Pusztai L, **Coombes KR**. The bimodality index. a
criterion for discovering and ranking bimodal signatures from cancer gene
expression profiling data. Cancer Inform. 2009 Aug 5;7:199-216. [PMID](https://www.ncbi.nlm.nih.gov/pubmed/19718451); PMCID. PMC2730180.
1. Zhang L, Wei Q, Mao L, Liu W, Mills GB, **Coombes K**. Serial dilution curve. a
new method for analysis of reverse phase protein array data. Bioinformatics. 2009
Mar 1;25(5):650-4. doi. 10.1093/bioinformatics/btn663. Epub 2009 Jan 28. PubMed
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/19176552); PMCID. PMC2647837.
1. **Gold D**, Mallick B, **Coombes K**. Real-time gene expression. statistical
challenges in design and inference. J Comput Biol. 2008 Jul-Aug;15(6):611-23.
doi. 10.1089/cmb.2007.0220. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/18631024).
1. Baggerly KA, **Coombes KR**, Neeley ES. Run batch effects potentially compromise 
the usefulness of genomic signatures for ovarian cancer. J Clin Oncol. 2008 Mar
1;26(7):1186-7; author reply 1187-8. doi. 10.1200/JCO.2007.15.1951. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/18309960).
1. McDonnell TJ, Chari NS, Cho-Vega JH, Troncoso P, Wang X, Bueso-Ramos CE,
**Coombes K**, Brisbay S, Lopez R, Prendergast G, Logothetis C, Do KA. Biomarker
expression patterns that correlate with high grade features in treatment naive,
organ-confined prostate cancer. BMC Med Genomics. 2008 Jan 31;1:1. doi:
10.1186/1755-8794-1-1. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/18237448); PMCID. PMC2227949.
1. **Coombes KR**, **Wang J**, Baggerly KA. Microarrays. retracing steps. Nat Med. 2007 
Nov;13(11):1276-7; author reply 1277-8. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/17987014).
1. Abruzzo LV, Barron LL, Anderson K, Newman RJ, Wierda WG, O'brien S, Ferrajoli
A, Luthra M, Talwalkar S, Luthra R, Jones D, Keating MJ, **Coombes KR**.
Identification and validation of biomarkers of IgV(H) mutation status in chronic 
lymphocytic leukemia using microfluidics quantitative real-time polymerase chain 
reaction technology. J Mol Diagn. 2007 Sep;9(4):546-55. Epub 2007 Aug 9. PubMed
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/17690214); PMCID. PMC1975107.
1. Nakamura T, Fidler IJ, **Coombes KR**. Gene expression profile of metastatic
human pancreatic cancer cells depends on the organ microenvironment. Cancer Res. 
2007 Jan 1;67(1):139-48. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/17210693).
1. **Wang J**, Do KA, Wen S, Tsavachidis S, McDonnell TJ, Logothetis CJ, **Coombes KR**.
Merging microarray data, robust feature selection, and predicting prognosis in
prostate cancer. Cancer Inform. 2007 Feb 14;2:87-97. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/19458761);
PMCID. PMC2675498.
1. **Gold DL**, **Coombes KR**, **Wang J**, Mallick B. Enrichment analysis in
high-throughput genomics - accounting for dependency in the NULL. Brief
Bioinform. 2007 Mar;8(2):71-7. Epub 2006 Oct 31. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/17077137).
1. Ji Y, **Coombes K**, **Zhang J**, Wen S, Mitchell J, Pusztai L, Symmans WF, **Wang J**.
RefSeq refinements of UniGene-based gene matching improve the correlation of
expression measurements between two microarray platforms. Appl Bioinformatics.
2006;5(2):89-98. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/16722773).
1. **Zhang J**, Zhang L, **Coombes KR**. Gene sequence signatures revealed by mining the
UniGene affiliation network. Bioinformatics. 2006 Feb 15;22(4):385-91. Epub 2005 
Dec 8. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/16339286).
1. **Coombes KR**, Tsavachidis S, Morris JS, Baggerly KA, Hung MC, Kuerer HM.
Improved peak detection and quantification of mass spectrometry data acquired
from surface-enhanced laser desorption and ionization by denoising spectra with
the undecimated discrete wavelet transform. Proteomics. 2005 Nov;5(16):4107-17.
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/16254928).
1. Hu J, Kapoor M, Zhang W, Hamilton SR, **Coombes KR**. Analysis of dose-response
effects on gene expression data with comparison of two microarray platforms.
Bioinformatics. 2005 Sep 1;21(17):3524-9. Epub 2005 Aug 4. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/16081476).
1. **Gold DL**, **Wang J**, **Coombes KR**. Inter-gene correlation on oligonucleotide
arrays. how much does normalization matter? Am J Pharmacogenomics.
2005;5(4):271-9. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/16078863).
1. Abruzzo LV, **Wang J**, Kapoor M, Medeiros LJ, Keating MJ, Edward Highsmith W,
Barron LL, Cromwell CC, **Coombes KR**. Biological validation of differentially
expressed genes in chronic lymphocytic leukemia identified by applying multiple
statistical methods to oligonucleotide microarrays. J Mol Diagn. 2005
Aug;7(3):337-45. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/16049305); PMCID. PMC1867538.
1. Abruzzo LV, Lee KY, Fuller A, Silverman A, Keating MJ, Medeiros
LJ, **Coombes KR**. Validation of oligonucleotide microarray data using microfluidic low-density 
arrays. a new statistical method to normalize real-time RT-PCR data.
Biotechniques. 2005 May;38(5):785-92. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15945375).
1. Hu J, **Coombes KR**, Morris JS, Baggerly KA. The importance of experimental
design in proteomic mass spectrometry experiments. some cautionary tales. Brief
Funct Genomic Proteomic. 2005 Feb;3(4):322-31. Review. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15814023).
1. **Coombes KR**, Morris JS, Hu J, Edmonson SR, Baggerly KA. Serum proteomics
profiling--a young technology begins to mature. Nat Biotechnol. 2005
Mar;23(3):291-2. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15765078).
1. Baggerly KA, Morris JS, Edmonson SR, **Coombes KR**. Signal in noise. evaluating 
reported reproducibility of serum proteomic tests for ovarian cancer. J Natl
Cancer Inst. 2005 Feb 16;97(4):307-9. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15713966).
1. Ji Y, Wu C, Liu P, **Wang J**, **Coombes KR**. Applications of beta-mixture models in
bioinformatics. Bioinformatics. 2005 May 1;21(9):2118-22. Epub 2005 Feb 15.
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15713737).
1. **Coombes KR**, Koomen JM, Baggerly KA, Morris JS, Kobayashi R. Understanding the
characteristics of mass spectrometry data through the use of simulation. Cancer
Inform. 2005;1:41-52. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/19305631); PMCID. PMC2657656.
1. Morris JS, **Coombes KR**, Koomen J, Baggerly KA, Kobayashi R. Feature extraction
and quantification for mass spectrometry in biomedical applications using the
mean spectrum. Bioinformatics. 2005 May 1;21(9):1764-75. Epub 2005 Jan 26. PubMed
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15673564).
1. Baggerly KA, **Coombes KR**, Morris JS. Bias, randomization, and ovarian
proteomic data. a reply to "producers and consumers". Cancer Inform. 2005;1:9-14.
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/19305627); PMCID. PMC2657654.
1. **Coombes KR**. Analysis of mass spectrometry profiles of the serum proteome.
Clin Chem. 2005 Jan;51(1):1-2. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15613701).
1. Baggerly KA, Edmonson SR, Morris JS, **Coombes KR**. High-resolution serum
proteomic patterns for ovarian cancer detection. Endocr Relat Cancer. 2004
Dec;11(4):583-4; author reply 585-7. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15613439).
1. **Wang J**, **Coombes KR**, Highsmith WE, Keating MJ, Abruzzo LV. Differences in gene
expression between B-cell chronic lymphocytic leukemia and normal B cells. a
meta-analysis of three microarray studies. Bioinformatics. 2004 Nov
22;20(17):3166-78. Epub 2004 Jul 1. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15231529).
1. **Coombes KR**, Zhang L, Bueso-Ramos C, Brisbay S, Logothetis C, Roth J, Keating 
MJ, McDonnell TJ. TAD. a web interface and database for tissue microarrays. Appl 
Bioinformatics. 2002;1(3):155-8. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/15130842).
1. Baggerly KA, Morris JS, **Coombes KR**. Reproducibility of SELDI-TOF protein
patterns in serum. comparing datasets from different experiments. Bioinformatics.
2004 Mar 22;20(5):777-85. Epub 2004 Jan 29. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/14751995).
1. **Gold D**, **Coombes K**, Medhane D, Ramaswamy A, Ju Z, Strong L, Koo JS, Kapoor M. 
A comparative analysis of data generated using two different target preparation
methods for hybridization to high-density oligonucleotide microarrays. BMC
Genomics. 2004 Jan 6;5(1):2. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/14709180); PMCID:
PMC331399.
1. Morris JS, Baggerly KA, **Coombes KR**. Bayesian shrinkage estimation of the
relative abundance of mRNA transcripts using SAGE. Biometrics. 2003
Sep;59(3):476-86. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/14601748).
1. **Coombes KR**, Fritsche HA Jr, Clarke C, Chen JN, Baggerly KA, Morris JS, Xiao
LC, Hung MC, Kuerer HM. Quality control and peak finding for proteomics data
collected from nipple aspirate fluid by surface-enhanced laser desorption and
ionization. Clin Chem. 2003 Oct;49(10):1615-23. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/14500586).
1. Baggerly KA, Morris JS, **Wang J**, **Gold D**, Xiao LC, **Coombes KR**. A comprehensive 
approach to the analysis of matrix-assisted laser desorption/ionization-time of
flight proteomics spectra from serum samples. Proteomics. 2003 Sep;3(9):1667-72. 
[PMID.](https://www.ncbi.nlm.nih.gov/pubmed/12973722).
1. **Coombes KR**, Highsmith WE, Krogmann TA, Baggerly KA, Stivers DN, Abruzzo LV.
Identifying and quantifying sources of variation in microarray data using
high-density cDNA membrane arrays. J Comput Biol. 2002;9(4):655-69. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/12323099).
1. Baggerly KA, **Coombes KR**, Hess KR, Stivers DN, Abruzzo LV, Zhang W.
Identifying differentially expressed genes in cDNA microarray experiments. J
Comput Biol. 2001;8(6):639-59. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/11747617).
1. Hess KR, Zhang W, Baggerly KA, Stivers DN, **Coombes KR**. Microarrays. handling 
the deluge of data and extracting reliable information. Trends Biotechnol. 2001
Nov;19(11):463-8. Review. [PMID.](https://www.ncbi.nlm.nih.gov/pubmed/11602311).

