---
author: Kevin R. Coombes
date: 11 January 2018
title: Coombes Lab
---

This project will hold information about the Coombes lab.

* The [doc](./doc/) folder includes information about the tools you should
  install when joining the lab.
* The [doc/papers](./doc/papers/) folder lists publications.
* The [people](./people/) folder lists curent and former lab members.
* The [projects](./projects/) lists many of our GitLab and R-Forge projects.

