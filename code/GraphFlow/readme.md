---
author: Kevin R. Coombes
date: 18 December 2019
title: Graphic file dependencies in R scripts
---

# OVerview

1. hunter.pl STARTDIR  
    Produces a TSV file on standard output, with four columns 
	(Line Number, File, Verb, Value)
2. gatherer.pl INPUTFILE  
    Takes a TSV file as input (like the one coming from `hunter`)
	and produces a similar file on standard output, but replacing
	obvious substitutions.
3. makeNodes.pl INPUTFILE  
    Takes a TSV file produced by `gatherer` and converts it into a
    graphml file.
	
