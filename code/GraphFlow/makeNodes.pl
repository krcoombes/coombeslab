#! perl -w

use strict;
use warnings;

my $source = shift || "temp2.txt";
my $graphmlFile = $source;
$graphmlFile =~ s/txt/graphml/;

# pass 1: get nodes and edges
my %scripts = ();
my $nscript = 0;
my %rdata = ();
my $nr = 0;
my %txtdata = ();
my $ntxt = 0;
my @edges = ();
my $nedge = 0;
open(SRC, "<$source") or die "Unable to open '$source': $!\n";
while (my $line = <SRC>) {
    chomp $line;
    my ($lineno, $script, $command, $data) = split /\t/, $line;
    ++$nedge; # every row/line is an edge
    my %edge = (label => "E$nedge",
		type => $command);
    unless (defined($scripts{$script})) { # node for each unique script
	++$nscript;
	$scripts{$script} = "N$nscript";
    }

    if ($command eq "read") {       # text based files, input
	unless (defined($txtdata{$data})){
	    ++$ntxt;
	    $txtdata{$data} = "T$ntxt";
	}
	$edge{left} = $txtdata{$data};
	$edge{right} = $scripts{$script};
    } elsif ($command eq "write") { # text based files, output
	unless (defined($txtdata{$data})){
	    ++$ntxt;
	    $txtdata{$data} = "T$ntxt";
	}
	$edge{left} = $scripts{$script};
	$edge{right} = $txtdata{$data};
    } elsif ($command eq "load") { # r binary data files, input
	unless (defined($rdata{$data})){
	    ++$nr;
	    $rdata{$data} = "R$nr";
	}
	$edge{left} = $rdata{$data};
	$edge{right} = $scripts{$script};
    } elsif ($command eq "save") { # r binary data files, output
	unless (defined($rdata{$data})){
	    ++$nr;
	    $rdata{$data} = "R$nr";
	}
	$edge{left} = $scripts{$script};
	$edge{right} = $rdata{$data};;
    } else { # not good
	die "in agony\n";
    }
    push @edges, {%edge};
}
close(SRC);

# pass 2: actually make the graphml file
my %colHash = (
    scripts => "#2ED9FF", # lightskyblue
    rdata   => "#1CBE4F", # springreen3
    txtdata => "#FEAF16", # darkgoldenrod1
    other   => "#F6222E", # firebrick1
    );

my %fgHash = (
    scripts => "#000000",
    rdata   => "#000000",
    txtdata => "#000000",
    other   => "#000000",
    );

## GraphML outer structure
my $gmlHead = <<'ENDHEAD'
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:java="http://www.yworks.com/xml/yfiles-common/1.0/java" xmlns:sys="http://www.yworks.com/xml/yfiles-common/markup/primitives/2.0" xmlns:x="http://www.yworks.com/xml/yfiles-common/markup/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <!--Created by yEd 3.15.0.2-->
  <key attr.name="Description" attr.type="string" for="graph" id="d0"/>
  <key for="port" id="d1" yfiles.type="portgraphics"/>
  <key for="port" id="d2" yfiles.type="portgeometry"/>
  <key for="port" id="d3" yfiles.type="portuserdata"/>
  <key attr.name="url" attr.type="string" for="node" id="d4"/>
  <key attr.name="description" attr.type="string" for="node" id="d5"/>
  <key for="node" id="d6" yfiles.type="nodegraphics"/>
  <key for="graphml" id="d7" yfiles.type="resources"/>
  <key attr.name="url" attr.type="string" for="edge" id="d8"/>
  <key attr.name="description" attr.type="string" for="edge" id="d9"/>
  <key for="edge" id="d10" yfiles.type="edgegraphics"/>
  <graph edgedefault="directed" id="G">
    <data key="d0"/>
ENDHEAD
    ;
my $gmlTail = <<'ENDTAIL'
  </graph>
  <data key="d7">
    <y:Resources/>
  </data>
</graphml>
ENDTAIL
    ;


## Node Structure
my $beginNode =<<'BNODE'
      <data key="d5"/>
      <data key="d6">
        <y:ShapeNode>
          <y:Geometry height="30.0" width="100.033203125" x="467.9833984375" y="91.0"/>
          <y:Fill color=
BNODE
    ;
my $midNode =<<'MNODE'
 transparent="false"/>
          <y:BorderStyle color="#000000" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="18.701171875" modelName="custom" textColor=
MNODE
    ;
my $mid2node =<<'M2'
 visible="true" width="90.033203125" x="5.0" y="5.6494140625">
M2
    ;
my $endNode =<<'ENODE'
<y:LabelModel>
              <y:SmartNodeLabelModel distance="4.0"/>
            </y:LabelModel>
            <y:ModelParameter>
              <y:SmartNodeLabelModelParameter labelRatioX="0.0" labelRatioY="0.0" nodeRatioX="0.0" nodeRatioY="0.0" offsetX="0.0" offsetY="0.0" upX="0.0" upY="-1.0"/>
            </y:ModelParameter>
          </y:NodeLabel>
          <y:Shape type="roundrectangle"/>
        </y:ShapeNode>
      </data>
    </node>
ENODE
    ;

## Edge Structure
#    <edge id="e0" source="n112" target="n47">
my $makeEdge = <<'MEDGE'
      <data key="d9"/>
      <data key="d10">
        <y:PolyLineEdge>
          <y:Path sx="0.0" sy="0.0" tx="0.0" ty="0.0"/>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
MEDGE
    ;


open(GML, ">$graphmlFile") or die "Unable to create '$graphmlFile': $!\n";
print GML $gmlHead;

# script nodes
foreach my $key (keys %scripts) {
    my $nodeID = $scripts{$key};
    my $nodeNumber = "    <node id=\"$nodeID\">\n";
    my $nodeColor = $colHash{scripts};
    my $txtColor = $fgHash{scripts};
    print GML $nodeNumber, $beginNode, "\"$nodeColor\"", 
	$midNode, "\"$txtColor\"", $mid2node,
	$key, $endNode;
}

# R data nodes
foreach my $key (keys %rdata) {
    my $nodeID = $rdata{$key};
    my $nodeNumber = "    <node id=\"$nodeID\">\n";
    my $nodeColor = $colHash{rdata};
    my $txtColor = $fgHash{rdata};
    print GML $nodeNumber, $beginNode, "\"$nodeColor\"", 
	$midNode, "\"$txtColor\"", $mid2node,
	$key, $endNode;
}

# txt data nodes
foreach my $key (keys %txtdata) {
    my $nodeID = $txtdata{$key};
    my $nodeNumber = "    <node id=\"$nodeID\">\n";
    my $nodeColor = $colHash{txtdata};
    my $txtColor = $fgHash{txtdata};
    print GML $nodeNumber, $beginNode, "\"$nodeColor\"", 
	$midNode, "\"$txtColor\"", $mid2node,
	$key, $endNode;
}


#    <edge id="e0" source="n112" target="n47">
foreach my $eref (@edges) {
    my $caller = $eref->{left};
    my $callee = $eref->{right};
    my $id = $eref->{label};
    my $line = "   <edge id =\"$id\" source=\"$caller\" target=\"$callee\">\n";
    print GML $line, $makeEdge;
}

print GML $gmlTail;
close(GML);

exit;
__END__
