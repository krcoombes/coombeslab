#! perl -w

use strict;
use warnings;

my $debug = 1;

my $source = shift or die "You must provide a filename!\n";
open(SRC, "<$source") or die "Unable to open '$source': $!\n";
my $file = '';
my %hash = ();
while (my $line = <SRC>) {
    chomp $line;
    my @items = split /\t/, $line; # tsv format: lineno file command args.
    if ($file ne $items[1]) { # start over with each new file
	%hash = (); # clear the hash
	$file = $items[1];
	print STDERR "Clearing hash\n" if $debug >= 2;
    }
    my $cmd = $items[2];
    if ($cmd =~ /^[fgh]/) { # defines a short variable to avoid retyping
	print STDERR "Adding '$cmd' to hash\n" if $debug >= 2;
	$hash{$cmd} = $items[3];
	next;
    }
    $cmd = "read" if $cmd =~ /read/;
    $cmd = "write" if $cmd =~ /write/;
    $items[2] = $cmd;
    # Now we must be a load, save, read, or write
    # Want to pull out "file=" from the args
    my $arg = $items[3];
    # remove spaces around equal signs
    $arg =~ s/\s+=/=/g;
    $arg =~ s/=\s+/=/g;
    if ($arg =~ /file=(.*?)$/) { # won't try to parse extra arguments
	$arg = $1;
    }
    unless(my $size = scalar(keys(%hash))) { # just write things out if no hash entries
	$items[3] = xform($arg);
	$line = join("\t", @items);
	print "$line\n";
	next;
    }
    # Now we may have to make substitutions
    print STDERR "Checking substitutions for '$arg'\n" if $debug >= 2;
    if ($cmd =~ "read") { # e.g., read.csv, read.table , etc.
	$arg = "LOCALSRC"; # default for first study.
    } else {
	my $value = $hash{$arg};
	if (defined($value)) {
	    my $oarg = $arg;
	    $arg = $value;
	}
	$arg = xform($arg);
    }
    print "$items[0]\t$items[1]\t$items[2]\t$arg\n";
}
close(SRC);

sub xform {
    my $arg = shift;
    if ($arg =~ /^(.*?)paste\((.*?),\s*sep\s?=\s?\"(.*?)\"\)(.*?)$/) {
	my $pre = $1;
	my $guts = $2;
	my $sep = $3;
	my $post = $4;
	$guts = join($sep, split /,/, $guts);
	$arg = $pre.$guts.$post;
    }
    if ($arg =~ /file.path\((.*?)\)/) {
	my @bits = split /,/, $1;
	$arg = join("/", @bits);
    } elsif ($arg =~ /path/) {
	print STDERR "Weirder: '$arg'\n";
    }
    $arg =~ s/\s//g; #remove white space
    $arg =~ s/"//g; # remove quration marks
    $arg;
}

exit;
__END__
