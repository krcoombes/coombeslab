#! perl -w

# HUNTER.PL

# PURPOSE
## to identify data inputs (via read or load commands)
## and outputs (via write or save commands) in one or more
## scripts running R. It should handle *.R, *.Rmd, and *.Rnw
## formats.

use strict;
use warnings;

my $startdir = shift || "."; # default is to start in the current directory
# initial version also assumes recursion into subdirectories

use File::Basename;
use Cwd;
use Cwd 'abs_path';
my $home = getcwd();
print STDERR "Home = $home\n";
$startdir = abs_path($startdir);

my %handlers = (".R"   => \&Rhandler,
		".Rnw" => \&Rnwhandler,
		".Rmd" => \&Rmdhandler);
my @ftypes = keys %handlers;
@ftypes = map {"\\".$_} @ftypes; # need to escape the "dot matches anything"
print STDERR join("\t", @ftypes), "\n";

my @stack = ($startdir);
while(my $active = pop(@stack)) {
    chdir($active) or die "Unable to change directory to '$active': $!\n";
#    print STDERR "Checking directory '$active'.\n";
    my @files = glob "*";
    foreach my $file (@files) {
	if (-d $file) { # push directories onto the stack
	    push @stack, "$active/$file";
	    next;
	}
	my ($fname, $ignore, $suffix) = fileparse($file, @ftypes);
	next unless $suffix;
	print STDERR "input = '$file', fname='$fname', suffix='$suffix'\n";
	my $handler = $handlers{$suffix};
	$handler->($file);
    }
    chdir $home;
}

sub clean {
    my $line = shift;
    my $oline = $line;
    chomp $line;
    $line =~ s/^\s+|\s+$//; # strip initial and final white space
    $line;
}

sub doline {
    my $file = shift;
    my $lineno = shift;
    my $line = shift;
    my $fh = shift;
    if ($line =~ /^([fgh]\d*) <- (.*?)$/) {
	my $varb = $1;
	my $valu = $2;
	print "$lineno\t$file\t$varb\t$valu\n";
    } elsif ($line =~ /save\(/ | $line =~ /write/ | $line =~ /read\./ |
	     $line =~ /load\(/) {
	while($line !~ /\)$/) {
	    ++$lineno;
	    my $temp = <$fh>;
	    $line = $line.clean($temp);
	}
	$line =~ s/ = /=/g;
	if ($line =~ /^(.*?)\((.*?)\)$/) {
	    my $cmd = $1;
	    my $args = $2;
	    print "$lineno\t$file\t$cmd\t$args\n";
	}
    }
    return $lineno;
}

sub knithandler {
    my $file = shift;
    my $lchunk = shift;
    my $rchunk = shift;
    open(RMD, "<$file") or die "Unable to open '$file': $!\n";
    my $lineno = 0;
    my $chunk = "text";
    while (my $line = <RMD>) {
	++$lineno;
	$line = clean($line);
	if ($chunk eq "text") {
	    if ($line =~ /$lchunk/) {
		$chunk = "code";
	    }
	    next
	}
	# to get here, must be in a code chunk
	if ($line =~ /$rchunk/) {
	    $chunk = "text";
	    next;
	}
	# still in a code chunk
	next if $line =~ /^#/; # skip comments
	$lineno = doline($file, $lineno, $line, \*RMD);
    }
    close(RMD);
}

sub Rhandler {
    my $file = shift;
    print STDERR "Handling R file: $file\n";
    open(R, "<$file") or die "Unable to open '$file': $!\n";
    my $lineno = 0;
    while (my $line = <R>) {
	++$lineno;
	$line = clean($line);
	next if $line =~ /^#/; # skip comments
	$lineno = doline($file, $lineno, $line, \*R);
    }
    close(R);
}

sub Rnwhandler {
    my $file = shift;
    print STDERR "Handling Rnw file: $file\n";
    knithandler($file, "^<<(.*)>>=", "^@ ")
}

sub Rmdhandler {
    my $file = shift;
    print STDERR "Handling Rmd file: $file\n";
    knithandler($file,  "^```\{r", "^```")
}


exit;
__END__
