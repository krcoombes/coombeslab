---
author: Kevin R. Coombes
date: 11 January 2018
title: Git Projects
---

https://code.bmi.osumc.edu/asiaeetaheri.1/imo

# Introduction

Git projects are located at two different hosts:

2. the [public GitLab](https://about.gitlab.com/) server, and
3. the [public GitHub](https://github.com/) server.

# Public Server GitLab Projects

1. [Kevin Coombes / BetaZif](https://gitlab.com/krcoombes/betazif)  
Project to use beta distributions to study the zero-inflation problem with application to miR-Seq data from TCGA. 

2. [Kevin Coombes / ClinicalClustering](https://gitlab.com/krcoombes/clinclust)  
Realistic simulations of clinical data to test clustering and other unsupervised machine learning algorithms.
3. [Kevin Coombes / CloneFinder](https://gitlab.com/krcoombes/clonefinder)  
Project to support manuscript describing and applying the CloneSeeker R package.
4. [Kevin Coombes / CoombesLab](https://gitlab.com/krcoombes/coombeslab)  
Project to hold start-up and other information for/about Kevin Coombes' lab.
5. [Kevin Coombes / cytangle](https://gitlab.com/krcoombes/cytangle)  
Analysis of mass cytometry (CyTOF) AML data.
6. [Kevin Coombes / CytoGPS](https://gitlab.com/krcoombes/cytogps)  
Papers and analyses that describe and apply the CytoGPS system to convert ISCN karyotypes into computer-friendly binary strings.
8. [Kevin Coombes / DrugScreeningAML](https://gitlab.com/krcoombes/drugscreeningaml)
3. [Kevin Coombes / HousekeepeR](https://gitlab.com/krcoombes/HousekeepeR)  
Collaborative project to identify reference (or "housekeeping") genes, based on TCGA RNA-Sequencing data, for use in quantitative real-time PCR experiments,
10. [Cat E. Coombes / ICUTimeCourse](https://gitlab.com/catacombs/timecourse)  
10. [Kevin Coombes / miRNAClusters](https://gitlab.com/krcoombes/mirnaclusters)  
Project to use TCGA data to explore relationships between mRNA and miRNA expression
1. [Kevin Coombes / ML4Bioinfo](https://gitlab.com/krcoombes/ML4Bioinfo)  
Machine Learning for Bioinformatics. Contains material for a course being taught at The Ohio State University in Autumn 2018.
11. [Kevin Coombes / NewmanOmics](https://gitlab.com/krcoombes/newmanomics)  
Project to support manuscript describing the Newman statistics.
2. [Kevin Coombes / Organizer](https://gitlab.com/krcoombes/organizer)  
Organizing research projects for collaboration and reproducibility.
12. [Kevin Coombes / Richters](https://gitlab.com/krcoombes/richters)  
Study of Richter's transformation (development of clonally related DLBCL in a patient with CLL)
13. [Kevin Coombes / SeqNorm](https://gitlab.com/krcoombes/seqnorm)  
Comparison of different methods to normalize RNA sequencing data.
14. [Kevin Coombes / SillyPutty](https://gitlab.com/krcoombes/sillyputty)  
Testing out a new idea for clustering focused on silhouette widths.
15. [Kevin Coombes / Stem Cell Karyotypes](https://gitlab.com/krcoombes/stem-cell-karyotypes)  
Analysis of 15,000 stem cell karyotpes from WiCell.
15. [Kevin Coombes / Thresher-TransFac](https://gitlab.com/krcoombes/thresher-transfac)  
Application of the Thresher method to understand transcription factor expression and influence in TCGA data
16. [Kevin Coombes / Trisomy12-SingleGene](https://gitlab.com/krcoombes/trisomy12-singlegene)  
Devleop an analysis scheme to analyze one gene at a time from the trisomy 12 CLL expression array project.

# Public Server GitHub Projects

1. [aasiaeet / DataEnrichment](http://github.com/aasiaeet/DataEnrichment)
2. [aasiaeet / InferringMutationsOrder](http://github.com/aasiaeet/InferringMutationsOrder)
3. [aasiaeet / SCH](http://github.com/aasiaeet/SCH)
4. [blachlyhub / mbdb](http://github.com/blachlylab/mbdb)
