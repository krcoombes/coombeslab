---
author: Cat Coombes
title: How to package in R Studio
date: 2020-04-28
---

# HOW TO BUILD AN EXISTING PACKAGE IN RSTUDIO
For a package downloaded from a version control client (ie. Git or SVN):

**Before you start**, save every active window you have in R studio. When you create
a new project (below), a new pane in RStudio will open. It may close any panes
you already had open, without warning or saving.

## Install devtools from CRAN
	install.packages(devtools)

## First, create a new R Project.
	File -> New Project -> Existing Directory ->
	Select a project working directory for the packagefrom within the downloaded package.
	For BinaryMatrix, I selected the subdirectory "BinaryMatrix" than contained the whole package.
	Create the project. 

Clicking "Build" on the header will give you the option to configure build tools.
	Under "Project build tools" select "Package" from the dropdown, and click "OK."
	
	You can reconfigure build tools in the future under Build tap in the header.

For your first build of the package, under "Build," click "Load All."

# OTHER RESOURCES
You can find more information here:

https://support.rstudio.com/hc/en-us/articles/200486488?version=1.1.463&mode=desktop
This has more information on how to create a new package, and many more helpful links.

