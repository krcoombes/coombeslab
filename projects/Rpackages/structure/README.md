---
author: Kevin R. Coombes
title: R Package Structure
date: 2018-06-04
---

# Overview

R requires certain directories and files (with specific names) to be
present in order to build an R package. A complete description can be
found in the PDF help file _Writing R Extensions_ that ships with
every version of R.

The `RPackage.zip` file present in this directory can be used to set
up the default structure. To use this file, simply download it, extract
the contents, rename the top folder from `RPackage` to the name you
have chosen for your own package, and start editing the contents.

# Brief Structure Description

The files that we recommend (which includes all minimally required
elements) for an R package are:

+ The `DESCRIPTION` file: contains a set of key-value pairs providing
  metadata about the package.
+ The `NAMESPACE` file: lists the functions, methods, and classes that
  are imported from other packages for use by this package, as well as
  which functions, methods, and classes are provided (exported) by this
  package for users to employ.
+ A `NEWS` file: contains structured descriptions of changes,
  improvements, bug fixes, etc., for different package versions.
+ A `TODO` file: contains a list of planned enhancements or
  improvements to the package.
+ A file called `.Rbuildignore` that instructs the R build process not
  to include certain files from this directory.

The folders that we recommend (which includes all minimally required
elements) for an R package are:

+ The `R` directory: contains all R scripts that implement package
  features.
+ The `man` directory: contains all manual pages, which are written as
  `.Rd` (for R documentation) files. All exported items are required to
  be documented.
+ The `tests` directory: contains scripts that test portions of the
  code, along with saved copies of the expected output.
+ The `vignettes` directory: contains Sweave (`.Rnw`) and/or
  Rmarkdown/knitr (`.Rmd`) files that illustrate how one expects the
  package as a whole to be used.
+ The `data` directory: contains binary data (`.rda`) files with data
  sets provided by the package. Some other formats may be recognized.
+ The `inst` directory: contains any other files (such as examples)
  that the package author wants to make available.
+ The `src` directory: contains C or FORTRAN code, if it is needed to
  implement any of the package features.
