---
author: Kevin R. Coombes
title: Building R Packages
date: 2018-05-16
---

# Package Building Steps

If all of the tools and programs that you ned are in your path, then
you shoudl perform the following steps to build, check, and install an
R package on your system. 

These instructions assume that you are starting out one directory
above the location of your package. In particular, if you list the
contents of your current working directory, you should see a folder
called something like `MyPackage`, where that represents the actual
name of your package.

1. `R CMD build MyPackage`.  This step takes your package directory
   and stores it in a "source tarball". That is, you end up creating a
   file with a name like `MyPackage-1.0.0.tar.gz`. The numeric part
   changes, since that is the current version number given to your
   package. The `tar` point of the name comes from the ancient UNIX
   "tape archive" program (known as `tar`), which is used nowadays
   simply to bundle multiep files and difectories into a single
   thing. The `gz` part comes from the "GNU zip" commpression program
   that saves on disc space. You should note that this
   bundling-and-compressing is smart enough to leave out certain files
   that you do't really need or want in the final package.
2. `R CMD INSTALL --build MyPackage-1.0.0.tar.gz`. This step builds a
   binary version of the package suitable for installation on tour
   system. If your package include C or FORTRAN code, then that code
   gets compiled into an appropriate binary form. Also, manual pages
   and vigenttes get built during this step.
3. `R CMD check --as-cran MyPackage-1.0.0.tar.gz`. This step is
   *extremely important*. R will check all aspects of your package,
   making sure that required pieces are present, that the examples in
   the man pages work, that you've documented everything, etc. The
   developers of R have had years (literally) to discover a nearly
   infinite number of ways nthat R packages can go wrong, and they try
   to check for all of them. You will soon learn to _hate_ `R CMD
   check`. Mainly because you will spend so much time iteraitng back
   and forth between running it, trying to fix the errors, rebuilding,
   and rechecking.
4. `R CMD INSTALL MyPackage-1.0.0.tar.gz`. Finally, you can actually
   install the package, from source code, on your system. 

# Automating the Process
As you might guess, there are enough steps (and options that I haven't
bothered decribing) that you might want to automate the whoe thing. I
have a perl script, `makeRF.pl`, that does exactly that on a Windows
machine.

And at some point, I will describe here how to use that script....
