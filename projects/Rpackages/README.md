---
author: Kevin R. Coombes
title: Building R Packages
date: 2018-05-16
---

# Overview

This GitLab project provides instruction and some tools that help with
building your own R package.

# Microsoft Windows

## Absolutely Required
You must obtain and install copies of:

* The latest version of R, from
  [CRAN](https://cran.r-project.org/). It is best to install this in a
  location whose path contains no spaces and which you can easily
  write to later. I usually use `C:\R\R-[VERSION]`, where `[VERSION]`
  gets replaced by the actual version number, which is something like
  3.5.0.
* The latest version of Rtools, also from
  [CRAN](https://cran.r-project.org/bin/windows/Rtools/). Again, you
  probably do not want to accept the default location. I usually
  install this in `C:\Rtools\[VERSION]`, where `[VERSION]` is something
  like 3.5. Note that the versions of R and Rtools must be
  compatible.
* A text editor that won't corrupt line endings or file
  names/extensions. Since I started out using a non-Windows machine a
  long time ago, I prefer
  [emacs](https://www.gnu.org/software/emacs/download.html#windows). People
  even more primitive than me prefer  `VIM`, for which I won't even
  deign to lookup a hyperlink. If by some chance you don't already
  have a favorite tex editor, do a google search for "programmers
  text editor windows free" and play around with them until you find
  one that you don't hate.
  
## Almost Absolutely Required
There are other tools/programs that you should install to make your
life easier. For these tools, you can just get the latest versions,
follow their instructions, and accept the default choices while
installing.

* A working version of perl. I use the 64-bit version of [Strawberry
  perl](http://strawberryperl.com/).
* A working version of TeX. I use the 64-bit version of
  [MikTeX](https://miktex.org/).
* A working version of GhostScript, I use the 64-bit version with the
  GNU Affero public license, available from the
  [GhostScript download page](https://www.ghostscript.com/download/gsdnld.html).
* A working version of QPDF. This can be obtained from their home page
  on [SourceForge](http://qpdf.sourceforge.net/).


## Optional
You can (and I do) also install a couple of optional tools:

* The latest version of [Pandoc](https://pandoc.org/).
* The latest version of
  [ImageMagick](https://www.imagemagick.org/script/index.php).
* The latest version of [GIMP](https://www.gimp.org/).
* The latest version of [RStudio](https://www.rstudio.com/).

# UNIX (LINUX and Macintosh)

Everything listed above (except for Rtools) started out as a UNIX
tool. So, you can go to most of the same places to obtain and install
versions of these tools on a LINUX box or Macintosh computer. How easy
any of this depends on whether or not you have administrator/superuser
rights on your machine. The Rtools stuff contains 

* A bunch of utilities that are native to UNIX anyway (make, bash,
  tar, grep, cat, diff, etc.)
* A C compiler based on GNU gcc 4.9.3

If you have A LINUX box, you probably already have these. I have no
idea what's pre-installed on a Mac.
